import {
  InterestTypes,
  Interests,
  MediaTypes,
  Privacy,
  Profile,
  SocialMedia,
} from "@prisma/client";

export const throttle = (() => {
  let isRunning = false;

  return (cb: () => void, time: number) => {
    if (isRunning) return;
    isRunning = true;

    cb();

    setTimeout(() => {
      isRunning = false;
    }, time);
  };
})();

export const getURLSearchParams = (url: string) => new URL(url).searchParams;

export const transformSocialsObjectDTO = (
  value: Record<string, string | undefined> | undefined,
  profileId: Profile["id"],
): Pick<SocialMedia, "link" | "type" | "profileId">[] =>
  value
    ? Object.entries(value)
        .filter(([, value]) => Boolean(value))
        .map(([key, value]) => ({
          profileId: profileId,
          link: value as string,
          type: key as MediaTypes,
        }))
    : [];

export const transformInterestsObjectDTO = (
  value: Record<string, boolean> | undefined,
  profileId: Profile["id"],
): Omit<Interests, "id">[] =>
  value
    ? Object.entries(value).map(([key]) => ({
        type: key as InterestTypes,
        selected: true,
        profileId: profileId,
      }))
    : [];

export const transformGeneralInfoObjectDTO = (
  value: Record<string, string | undefined> | undefined,
) => {
  if (!value) return {} as Partial<Profile>;

  const filteredInfo = Object.entries(value).filter(([, value]) =>
    Boolean(value),
  );

  return filteredInfo.length === 3
    ? ({
        ...Object.fromEntries(filteredInfo),
        profileFullFielded: true,
      } as Partial<Profile>)
    : (Object.fromEntries(filteredInfo) as Partial<Profile>);
};

export const transformPrivacyObjectDTO = (
  value: Record<string, boolean>,
): Pick<
  Privacy,
  "accountPrivate" | "showAge" | "showDescription" | "showLocation"
> =>
  value?.accountPrivate ||
  (!value?.showAge && !value?.showDescription && !value?.showLocation)
    ? {
        accountPrivate: true,
        showAge: false,
        showDescription: false,
        showLocation: false,
      }
    : {
        accountPrivate: false,
        showAge: Boolean(value?.showAge),
        showDescription: Boolean(value?.showDescription),
        showLocation: Boolean(value?.showLocation),
      };
