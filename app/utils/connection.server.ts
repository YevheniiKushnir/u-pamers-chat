import { parse } from "@conform-to/zod";

import { ConnectionDataType } from "~/types";

import {
  ConnectionQueryDataSchema,
  ConnectionQueryUserDataSchema,
} from "./schema";

export const allowedConnections = [
  process.env?.CONNECTION_KEQING_SITE,
  process.env?.LOCAL_CONNECTION_KEQING_SITE,
].filter(Boolean);

export const getConnectionDataFromQuery = (query: URLSearchParams) => {
  const submission = parse(query, {
    schema: ConnectionQueryDataSchema,
  });

  return submission.value;
};

export const getConnectionUserDataFromQuery = (query: URLSearchParams) => {
  const submission = parse(query, {
    schema: ConnectionQueryUserDataSchema,
  });

  return submission.value;
};

export const validateConnectionOrigin = (origin: string): boolean => {
  return allowedConnections.includes(origin);
};

export const createConnectionRedirectUrl = (
  selfRedirectUrl: ConnectionDataType["selfRedirectUrl"],
  connectionId?: string,
) => {
  if (!connectionId) {
    return selfRedirectUrl;
  }

  const searchParams = new URLSearchParams();

  searchParams.append("connectionId", connectionId);

  return `${selfRedirectUrl}?${searchParams.toString()}`;
};
