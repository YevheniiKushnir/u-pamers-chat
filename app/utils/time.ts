export const getHoursWithMinuts = (time: Date) => {
  const date = new Date(time);

  return `${date.getUTCHours()}:${date.getUTCMinutes()}`;
};

export const gerMessageTime = (time: Date | undefined) => {
  if (!time) return "...";

  const today = new Date();
  const messageDateTime = new Date(time);

  const timeDiff = Math.abs(today.getTime() - messageDateTime.getTime());
  const diffDays = Math.ceil(timeDiff / (1000 * 60 * 60 * 24));

  if (diffDays === 0) {
    const formattedTime = messageDateTime.toLocaleTimeString("en-US", {
      hour: "2-digit",
      minute: "2-digit",
    });
    return formattedTime;
  } else if (diffDays <= 6) {
    const formattedDate = messageDateTime.toLocaleString("en-US", {
      weekday: "short",
      hour: "2-digit",
      minute: "2-digit",
    });
    return formattedDate;
  } else {
    const formattedDate = messageDateTime.toLocaleDateString("en-US", {
      month: "2-digit",
      day: "2-digit",
      year: "numeric",
    });
    return formattedDate;
  }
};
