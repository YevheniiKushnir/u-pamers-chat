import { Message } from "@prisma/client";

import { GroupedByDateMessages, GroupedByUserMessages } from "~/types";

const getMessageInfo = (message: Message) => ({
  userId: message.userId,
  isSended: message.isSended,
  isReaded: message.isReaded,
  createdAt: message.createdAt,
});

export const groupMessagesByDate = (
  messages: Message[],
): GroupedByDateMessages[] => {
  const groupedByDate = messages.reduce((acc: Record<string, Message[]>, m) => {
    const date = new Date(m.createdAt).toDateString();

    if (date in acc) {
      acc[date] = [...acc[date], m];
    } else {
      acc[date] = [m];
    }

    return acc;
  }, {});

  return Object.entries(groupedByDate);
};

export const groupMessagesByUser = (
  messages: Message[],
): GroupedByUserMessages[] => {
  const groupedByUser = messages.reduce((acc: GroupedByUserMessages[], m) => {
    const lastUserMessage = acc.at(-1);

    if (lastUserMessage && lastUserMessage.userId === m.userId) {
      acc.splice(acc.length - 1, 1, {
        ...getMessageInfo(m),
        messages: [...lastUserMessage.messages, m],
      });
    } else {
      acc.push({
        ...getMessageInfo(m),
        messages: [m],
      });
    }

    return acc;
  }, []);

  return groupedByUser;
};
