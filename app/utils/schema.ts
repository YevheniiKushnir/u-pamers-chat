import { InterestTypes, MediaTypes } from "@prisma/client";
import { z } from "zod";

const emailSchema = z.string().email({ message: "This field cannot be empty" });
const firstNameSchema = z
  .string()
  .min(1, { message: "This field cannot be empty" });
const lastNameSchema = z
  .string()
  .min(1, { message: "This field cannot be empty" });
const passwordSchema = z
  .string()
  .min(8, { message: "Min length 8 characters" });

export const UserPasswordSchema = z
  .object({
    password: passwordSchema,
    confirm: passwordSchema,
  })
  .refine((data) => data.password === data.confirm, {
    message: "Passwords don't match",
    path: ["confirm"],
  });

export const UserCredSchema = z.object({
  email: emailSchema,
  firstName: firstNameSchema,
  lastName: lastNameSchema,
  password: UserPasswordSchema,
});

export const MAX_UPLOAD_SIZE = 1024 * 1024 * 3; // 3MB

export const ImageSchema = z.object({
  avatar: z
    .instanceof(File)
    .refine((file) => {
      return file.size <= MAX_UPLOAD_SIZE;
    }, "File size must be less than 3MB")
    .or(z.string())
    .optional(),
});

export const UploadedImageSchema = z.object({
  avatar: z.string(),
});

export const SocialsSchema = z.object({
  [MediaTypes.Facebook]: z.string().optional(),
  [MediaTypes.Instagram]: z.string().optional(),
  [MediaTypes.Linkedin]: z.string().optional(),
  [MediaTypes.Skype]: z.string().optional(),
  [MediaTypes.Telegram]: z.string().optional(),
});

export const UserSigninCredSchema = z.object({
  email: emailSchema,
  password: z.string().min(8, { message: "Min length 8 characters" }),
});

export const ChatSchema = z.object({
  message: z
    .string()
    .min(1, "Min length is 1 character")
    .max(1000, "Max length in 1000 characters"),
});

export const SettingsAccountSchema = z.object({
  email: emailSchema,
  firstName: firstNameSchema,
  lastName: lastNameSchema,
});

export const SettingsPasswordSchema = z.object({
  password: UserPasswordSchema,
  currentPassword: passwordSchema,
});

export const SettingsProfileDetailsSchema = z.object({
  generalInfo: z
    .object({
      birthdate: z.string(),
      location: z.string(),
      about: z.string(),
    })
    .optional(),
  socialMedia: SocialsSchema.optional(),
  interests: z
    .object({
      [InterestTypes.Attractions]: z.boolean().optional(),
      [InterestTypes.Dining]: z.boolean().optional(),
      [InterestTypes.Education]: z.boolean().optional(),
      [InterestTypes.Family]: z.boolean().optional(),
      [InterestTypes.Office]: z.boolean().optional(),
      [InterestTypes.Health]: z.boolean().optional(),
      [InterestTypes.Promotions]: z.boolean().optional(),
      [InterestTypes.Sports]: z.boolean().optional(),
      [InterestTypes.Travel]: z.boolean().optional(),
    })
    .optional(),
});

export const SettingsPrivacySchema = z.object({
  accountPrivate: z.boolean().optional(),
  showAge: z.boolean().optional(),
  showLocation: z.boolean().optional(),
  showDescription: z.boolean().optional(),
});

export const ConnectionQueryDataSchema = z.object({
  userName: z.string(),
  selfConnectionId: z.string(),
  selfRedirectUrl: z.string(),
  selfName: z.string(),
});

export const ConnectionQueryUserDataSchema = z.object({
  connectionId: z.string(),
  selfConnectionId: z.string(),
});
