import type { LinksFunction, LoaderFunctionArgs } from "@remix-run/node";
import { json } from "@remix-run/node";
import {
  Links,
  LiveReload,
  Meta,
  Outlet,
  Scripts,
  ScrollRestoration,
  useLoaderData,
} from "@remix-run/react";

import logoImg from "~/assets/images/logo.png";
import stylesheet from "~/tailwind.css";
import { getUser } from "~/utils/session.server";

import { getEnv } from "./utils/env.server";

export const links: LinksFunction = () => [
  { rel: "stylesheet", href: stylesheet },
  { rel: "icon", href: logoImg },
];

export const loader = async ({ request }: LoaderFunctionArgs) => {
  return json({ user: await getUser(request), env: getEnv() });
};

export default function App() {
  const loaderData = useLoaderData<typeof loader>();

  return (
    <html lang="en" className="h-full">
      <head>
        <meta charSet="utf-8" />
        <meta name="viewport" content="width=device-width,initial-scale=1" />
        <Meta />
        <Links />
      </head>
      <body className="h-full">
        <Outlet />
        <ScrollRestoration />
        <Scripts />
        <LiveReload port={Number(loaderData.env.REMIX_DEV_SERVER_WS_PORT)} />
      </body>
    </html>
  );
}
