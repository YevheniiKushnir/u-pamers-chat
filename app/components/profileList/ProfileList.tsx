import { FC } from "react";

import { UserWithSocials } from "~/types";

import FilterEmptyList from "../filter/FilterEmptyList";

import ProfileCard from "./ProfileCard";

const ProfileList: FC<{ users: UserWithSocials[] }> = ({ users }) => {
  if (!users.length) {
    return <FilterEmptyList />;
  }

  return (
    <div className="px-4 py-6 flex flex-col gap-3">
      {users.map((u) => (
        <ProfileCard key={u.id} user={u} />
      ))}
    </div>
  );
};

export default ProfileList;
