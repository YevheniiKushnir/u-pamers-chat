import { Form } from "@remix-run/react";
import { FC } from "react";

import MessageIcon from "~/assets/icons/MessageIcon";
import useIsSubmitting from "~/hooks/useIsSubmitting";
import { UserWithSocials } from "~/types";

import SocialMediaList from "../socialMediaList/SocialMediaList";

const ProfileCard: FC<{ user: UserWithSocials }> = ({ user }) => {
  const isSubmitting = useIsSubmitting();

  return (
    <div className="p-4 flex items-start gap-4 justify-between bg-white rounded-xl shadow">
      <div className="flex items-start gap-3">
        {user.photo ? (
          <img
            src={user.photo}
            alt="user avatar"
            className="w-12 h-12 rounded-full border border-slate-200 object-cover"
          />
        ) : (
          <div className="w-12 h-12 rounded-full border border-slate-200" />
        )}
        <div>
          <h5 className="text-black text-lg font-bold leading-snug">
            {user.firstName} {user.lastName}
          </h5>
          <p className="text-gray-500 text-sm font-medium leading-tight">
            Country, City
          </p>
          <SocialMediaList socialMedias={user.profile?.socialMedias ?? []} />
        </div>
      </div>
      <Form method="POST">
        <input hidden type="text" name="userId" defaultValue={user.id} />
        <button
          className="p-2 bg-indigo-500 disabled:bg-indigo-200 rounded-lg justify-center items-center gap-2 flex"
          disabled={isSubmitting}
          type="submit"
        >
          <MessageIcon />
        </button>
      </Form>
    </div>
  );
};

export default ProfileCard;
