import { Message, User } from "@prisma/client";
import { FC, ReactNode } from "react";

import { cn } from "~/utils/styles";

import Loader from "../ui/loader/Loader";

import MessageTime from "./components/MessageTime";

interface MessageItemProps {
  photo?: User["photo"];
  messages: Message[];
  isUser: boolean;
  isTyping: boolean;
}

interface MessageItemTextProps {
  children: ReactNode;
  isUser: boolean;
  id: string;
}

const MessageItemText: FC<MessageItemTextProps> = ({
  id,
  isUser,
  children,
}) => (
  <li
    key={id}
    className={cn("w-fit", {
      "ml-auto": isUser,
    })}
  >
    <p
      className={cn(
        "py-3 whitespace-pre-wrap text-sm font-medium leading-tight shadow-sm",
        {
          "pr-3 pl-3 text-black bg-white rounded-tl-xl rounded-tr-xl rounded-bl-sm rounded-br-xl":
            !isUser,
          "pl-3 pr-3 text-white bg-indigo-500 rounded-tl-xl rounded-tr-xl rounded-bl-xl rounded-br-sm":
            isUser,
        },
      )}
    >
      {children}
    </p>
  </li>
);

const MessageItem: FC<MessageItemProps> = ({
  messages,
  photo,
  isUser,
  isTyping,
}) => {
  return (
    <div>
      <div
        className={cn("flex gap-1", {
          "flex-row-reverse": isUser,
          "mt-5": isTyping,
        })}
      >
        {photo ? (
          <img
            src={photo}
            alt="user avatar"
            className="w-6 h-6 rounded-full border border-slate-200 object-cover self-end"
          />
        ) : (
          <div className="w-6 h-6 rounded-full border border-slate-200" />
        )}
        <ul className="flex flex-col gap-2 max-w-[75%]">
          {isTyping ? (
            <MessageItemText id="user-typing" isUser={isUser}>
              <Loader />
            </MessageItemText>
          ) : (
            messages.map(({ id, message }) => (
              <MessageItemText key={id} id={id} isUser={isUser}>
                {message}
              </MessageItemText>
            ))
          )}
        </ul>
      </div>
      <MessageTime time={messages.at(-1)?.createdAt} isUser={isUser} />
    </div>
  );
};

export default MessageItem;
