import { User } from "@prisma/client";
import { FC } from "react";

import { GroupedByDateMessages, GroupedByUserMessages } from "~/types";

import MessageItem from "./MessageItem";

interface MessageListProps {
  messagesByDate: GroupedByDateMessages<GroupedByUserMessages>[];
  users: Record<User["id"], User>;
  userId: User["id"];
}

const MessageList: FC<MessageListProps> = ({
  messagesByDate,
  users,
  userId,
}) => {
  return (
    <div className="flex flex-col gap-8">
      {messagesByDate.map(([groupKey, messages]) => (
        <div key={groupKey}>
          <h5 className="text-center text-gray-500 text-base font-semibold leading-tight">
            {groupKey}
          </h5>
          <div className="mt-4 flex flex-col gap-5">
            {messages.map((m, idx) => (
              <MessageItem
                key={`${m.userId}-${idx}`}
                photo={users[m.userId].photo}
                messages={m.messages}
                isUser={userId === m.userId}
                isTyping={false}
              />
            ))}
          </div>
        </div>
      ))}
    </div>
  );
};

export default MessageList;
