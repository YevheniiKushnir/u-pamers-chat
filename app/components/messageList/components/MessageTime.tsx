import { FC } from "react";

import { cn } from "~/utils/styles";
import { getHoursWithMinuts } from "~/utils/time";

interface MessageTimeProps {
  time: Date | undefined;
  isUser: boolean;
}

const MessageTime: FC<MessageTimeProps> = ({ time, isUser }) => {
  return time ? (
    <p
      className={cn("mt-1 text-gray-500 text-xs font-normal leading-none", {
        "text-right mr-7": isUser,
        "text-left ml-7": !isUser,
      })}
    >
      {getHoursWithMinuts(time)}
    </p>
  ) : null;
};

export default MessageTime;
