import { User } from "@prisma/client";
import { FC } from "react";

import MessageItem from "../MessageItem";

interface MessageTypingType {
  photo?: User["photo"];
  isTyping: boolean;
}
const MessageTyping: FC<MessageTypingType> = ({ photo, isTyping }) => {
  if (!isTyping) return null;

  return <MessageItem isTyping isUser={false} messages={[]} photo={photo} />;
};

export default MessageTyping;
