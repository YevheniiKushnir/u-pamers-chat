// import CrossIcon from "~/assets/icons/CrossIcon";
import FilterIcon from "~/assets/icons/FilterIcon";

import InputSearch from "../ui/inputSearch/InputSearch";

const Filter = () => {
  return (
    <section className="bg-white px-4 py-6 shadow">
      <div className="container max-w-4xl mx-auto">
        <div className="flex items-center gap-5">
          <InputSearch sx="flex-1" />
          <button
            className="p-2 rounded-lg border border-indigo-500 justify-center items-center gap-2 flex relative disabled:bg-gray-200 disabled:border-indigo-200 [&disabled]:border-indigo-200"
            aria-label="filter"
            disabled
          >
            {/* <span className="absolute -top-0.5 -right-0.5 w-2 h-2 rounded-3xl bg-red-500 shadow shadow-red-400" /> */}
            <FilterIcon />
          </button>
        </div>
        {/* <div className="flex flex-wrap gap-3 mt-5">
          <div className="px-3 py-2 bg-indigo-50 rounded-3xl border border-indigo-500 justify-start items-center gap-2 flex">
            <p className="text-black">26-40 years</p>
            <button aria-label={`remove 26-40 years filter option`}>
              <CrossIcon svgProps={{ className: "w-4 h-4" }} />
            </button>
          </div>
        </div> */}
      </div>
    </section>
  );
};

export default Filter;
