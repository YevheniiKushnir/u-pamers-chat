import BigLoopIcon from "~/assets/icons/BigLoopIcon";

const FilterEmptyList = () => {
  return (
    <div className="flex flex-col items-center justify-center py-24">
      <BigLoopIcon />
      <h4 className="text-center text-black text-lg font-bold leading-snug">
        No U-Pamers found
      </h4>
      <p className="text-center text-gray-500 text-base font-medium leading-normal">
        Try another search query or remove filters.
      </p>
    </div>
  );
};

export default FilterEmptyList;
