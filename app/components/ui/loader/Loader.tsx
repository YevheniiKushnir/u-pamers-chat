const Loader = () => {
  return (
    <div className="w-14">
      <div className="dot-flashing" aria-label="loading">
        <span></span>
      </div>
    </div>
  );
};

export default Loader;
