import { Link, LinkProps } from "@remix-run/react";
import { FC, ReactNode } from "react";

import ArrowLeftIcon, {
  ArrowLeftIconProps,
} from "~/assets/icons/ArrowLeftIcon";

interface LinkBackProps extends LinkProps, ArrowLeftIconProps {
  children: ReactNode;
}

const LinkBack: FC<LinkBackProps> = ({
  svgProps,
  pathProps,
  children,
  ...props
}) => {
  return (
    <Link
      className="text-sky-600 text-base font-medium leading-tight flex items-center"
      {...props}
    >
      <ArrowLeftIcon svgProps={svgProps} pathProps={pathProps} /> {children}
    </Link>
  );
};

export default LinkBack;
