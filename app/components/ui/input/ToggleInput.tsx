import { FC, HTMLAttributes, useEffect, useState } from "react";

import { cn } from "~/utils/styles";

interface ToggleInputProps
  extends Omit<HTMLAttributes<HTMLInputElement>, "onChange"> {
  handleChange?: ((value: boolean) => void) | ((value: boolean) => void)[];
  disabled?: boolean;
  name?: string;
}

const ToggleInput: FC<ToggleInputProps> = ({
  defaultChecked,
  handleChange,
  disabled,
  name,
  ...props
}) => {
  const [checked, setChecked] = useState(defaultChecked ?? false);

  useEffect(() => {
    if (!defaultChecked) return;
    setChecked(defaultChecked);
  }, [defaultChecked]);

  return (
    <button
      className={cn("min-w-12 h-6 relative rounded-3xl flex items-center", {
        "bg-indigo-500": checked && !disabled,
        "bg-gray-400": !checked || Boolean(disabled),
      })}
      type="button"
      disabled={disabled}
      onClick={() => {
        const newChecked = !checked;

        if (Array.isArray(handleChange)) {
          handleChange.forEach((cb) => cb(newChecked));
        } else {
          handleChange?.(newChecked);
        }

        setChecked(newChecked);
      }}
    >
      <input
        type="checkbox"
        className="hidden"
        name={name}
        {...props}
        checked={disabled ? false : checked}
        readOnly
      />
      <div
        className={cn("w-3 h-3 mx-1 absolute bg-white rounded-full", {
          "right-0": checked && !disabled,
          "left-0": !checked || Boolean(disabled),
        })}
      />
    </button>
  );
};

export default ToggleInput;
