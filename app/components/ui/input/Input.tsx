import {
  FC,
  InputHTMLAttributes,
  LabelHTMLAttributes,
  forwardRef,
} from "react";

import ErrorIcon from "~/assets/icons/ErrorIcon";
import { cn } from "~/utils/styles";

type InputProps = InputHTMLAttributes<HTMLInputElement>;
interface LabelProps extends LabelHTMLAttributes<HTMLLabelElement> {
  required?: boolean;
}
interface ErrorListProps {
  id?: string;
  errors?: string[] | null;
}
interface InputFieldProps extends Pick<ErrorListProps, "errors">, InputProps {
  label: string;
  id?: string;
}

export const ErrorList: FC<ErrorListProps> = ({ id, errors }) => {
  return errors?.length ? (
    <ul id={id} className="flex flex-col gap-1">
      {errors.map((error, i) => (
        <li
          key={i}
          className="text-red-500 text-xs font-normal leading-none flex gap-1"
        >
          <ErrorIcon /> {error}
        </li>
      ))}
    </ul>
  ) : null;
};

export const Label: FC<LabelProps> = ({
  children,
  className,
  required,
  ...props
}) => (
  <label
    {...props}
    className={cn("text-gray-500 text-base font-normal leading-tight", {
      [className as string]: Boolean(className),
    })}
  >
    {children}
    {required ? (
      <span className="text-red-500">*</span>
    ) : (
      <span> (Optional)</span>
    )}
  </label>
);

export const Input = forwardRef<HTMLInputElement, InputProps>(
  ({ className, ...props }, ref) => (
    <input
      {...props}
      ref={ref}
      className={cn(
        "text-black text-base font-normal leading-tight outline-none w-full",
        {
          [className as string]: Boolean(className),
        },
      )}
    />
  ),
);
Input.displayName = "Input";

const InputField = forwardRef<HTMLInputElement, InputFieldProps>(
  ({ label, id, errors, ...props }, ref) => {
    return (
      <div className="flex flex-col gap-1">
        <div
          className={cn(
            "h-13 p-4 bg-white rounded-lg border border-gray-400 flex items-center justify-start relative",
            {
              "border-2 border-red-500": Boolean(errors?.length),
            },
          )}
        >
          <Input id={id} {...props} ref={ref} className="peer" placeholder="" />
          <Label
            htmlFor={id}
            required={props.required}
            className="absolute peer-[&:not(:placeholder-shown)]:-top-0 peer-[&:not(:placeholder-shown)]:text-xs peer-[&:not(:placeholder-shown)]:leading-3"
          >
            {label}
          </Label>
        </div>
        <ErrorList errors={errors} id={id} />
      </div>
    );
  },
);
InputField.displayName = "InputField";

export default InputField;
