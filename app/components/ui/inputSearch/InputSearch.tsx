import { useSearchParams } from "@remix-run/react";
import { FC, InputHTMLAttributes, ChangeEventHandler } from "react";

import CrossIcon from "~/assets/icons/CrossIcon";
import LoopIcon from "~/assets/icons/LoopIcon";
import { SEARCH_KEY } from "~/constants";

interface InputSearchProps extends InputHTMLAttributes<HTMLInputElement> {
  sx?: string;
}

const InputSearch: FC<InputSearchProps> = ({ sx, ...props }) => {
  const [searchParams, setSearchParams] = useSearchParams();

  const handleChangeSearch: ChangeEventHandler<HTMLInputElement> = (event) => {
    if (event.target.value.length) {
      if (props?.onChange) {
        props?.onChange(event);
      }
      setSearchParams((prev) => {
        prev.set(SEARCH_KEY, event.target.value);
        return prev;
      });
    } else {
      handleResetSearch();
    }
  };

  const handleResetSearch = () => {
    setSearchParams((prev) => {
      prev.delete(SEARCH_KEY);
      return prev;
    });
  };

  return (
    <div
      className={`flex items-center gap-3 py-2 px-3 rounded-lg border border-gray-400 focus-within:shadow ${
        sx ?? ""
      }`}
    >
      <label htmlFor="input-search" aria-label="search">
        <LoopIcon />
      </label>
      <input
        placeholder="Search by name"
        type="text"
        id="input-search"
        className="flex-1 text-black placeholder:text-gray-500 text-base leading-tight outline-none"
        {...props}
        onChange={handleChangeSearch}
        value={searchParams.get(SEARCH_KEY) ?? ""}
      />
      {(props?.value as string)?.length ? (
        <button
          type="button"
          onClick={handleResetSearch}
          aria-label="reset value"
        >
          <CrossIcon />
        </button>
      ) : null}
    </div>
  );
};

export default InputSearch;
