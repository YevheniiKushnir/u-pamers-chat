import useIsSubmitting from "~/hooks/useIsSubmitting";

const SettingsSubmitButton = () => {
  const isSubmitting = useIsSubmitting();

  return (
    <button
      type="submit"
      className="mt-4 px-12 py-4 bg-indigo-500 rounded-lg justify-center items-center inline-flex text-white text-base font-semibold leading-tight disabled:bg-indigo-300"
      disabled={isSubmitting}
    >
      Save updates
    </button>
  );
};

export default SettingsSubmitButton;
