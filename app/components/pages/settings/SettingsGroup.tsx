import { FC, ReactNode } from "react";

interface SettingsGroupProps {
  groupName?: string;
  children: ReactNode;
}

const SettingsGroup: FC<SettingsGroupProps> = ({ groupName, children }) => {
  return (
    <section className="px-4 py-8 bg-white rounded-lg flex flex-col gap-4 shadow">
      {groupName ? (
        <h4 className="text-black text-lg font-bold leading-snug">
          {groupName}
        </h4>
      ) : null}
      {children}
    </section>
  );
};

export default SettingsGroup;
