import { Link } from "@remix-run/react";
import { FC, ReactNode } from "react";

import ArrowRightIcon from "~/assets/icons/ArrowRightIcon";

interface SettingsLinkPageProps {
  routeTo: string;
  children: ReactNode;
}

const SettingsLinkPage: FC<SettingsLinkPageProps> = ({ routeTo, children }) => {
  return (
    <Link
      to={routeTo}
      className="flex items-center gap-2 bg-white rounded-lg shadow justify-between p-4 hover:bg-gray-200 active:bg-gray-200"
    >
      <p className="text-black text-base font-semibold leading-tight flex-1">
        {children}
      </p>{" "}
      <ArrowRightIcon />
    </Link>
  );
};

export default SettingsLinkPage;
