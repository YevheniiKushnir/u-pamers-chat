import { FC, ReactNode } from "react";

import LinkBack from "~/components/ui/link/LinkBack";

interface SettingsHeaderProps {
  children: ReactNode;
  routeTo: string;
  routeName: string;
}

const SettingsHeader: FC<SettingsHeaderProps> = ({
  children,
  routeTo,
  routeName,
}) => {
  return (
    <section className="flex flex-col gap-4">
      <LinkBack to={routeTo}>{routeName}</LinkBack>
      <h2 className="text-zinc-800 text-2xl font-bold leading-9">{children}</h2>
    </section>
  );
};

export default SettingsHeader;
