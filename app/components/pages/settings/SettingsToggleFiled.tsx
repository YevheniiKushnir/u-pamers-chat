import { FC, ReactNode } from "react";

interface SettingsToggleFiledProps {
  text: string;
  children: ReactNode;
}

const SettingsToggleFiled: FC<SettingsToggleFiledProps> = ({
  children,
  text,
}) => (
  <div className="flex gap-2 items-center justify-between">
    <p className="text-neutral-950 text-base font-semibold leading-tight">
      {text}
    </p>
    {children}
  </div>
);

export default SettingsToggleFiled;
