import { Link } from "@remix-run/react";
import { FC } from "react";

interface ProfileAboutProps {
  text?: string;
}

const ProfileAbout: FC<ProfileAboutProps> = ({ text }) => {
  return (
    <div>
      <h4 className="text-black text-lg font-bold leading-snug">About me</h4>
      {text?.length ? (
        <p className="mt-4 text-black text-base font-medium leading-normal">
          {text}
        </p>
      ) : (
        <p className="mt-4 text-gray-500 text-sm font-medium leading-tight">
          <Link
            to="/settings/profile?edit=about"
            className="text-sky-600 text-sm font-medium leading-tight"
          >
            Edit profile about
          </Link>{" "}
          to fill this section.
        </p>
      )}
    </div>
  );
};

export default ProfileAbout;
