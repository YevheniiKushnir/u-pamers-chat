import { Interests } from "@prisma/client";
import { FC } from "react";

import InterestList from "~/components/interestList/InterestList";

interface ProfileInterestsProps {
  interests: Interests[];
}

const ProfileInterests: FC<ProfileInterestsProps> = ({ interests }) => {
  return (
    <div>
      <h4 className="text-black text-lg font-bold leading-snug">Interests</h4>
      <InterestList interests={interests} />
    </div>
  );
};

export default ProfileInterests;
