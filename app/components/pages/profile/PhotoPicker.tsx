import { User } from "@prisma/client";
import { Form, useFormAction } from "@remix-run/react";
import { FC, useEffect, useRef, useState } from "react";

import CheckIcon from "~/assets/icons/CheckIcon";
import CrossIcon from "~/assets/icons/CrossIcon";
import PenIcon from "~/assets/icons/PenIcon";
import useIsSubmitting from "~/hooks/useIsSubmitting";

interface PhotoPickerProps {
  photo: User["photo"];
}

const PhotoPicker: FC<PhotoPickerProps> = ({ photo }) => {
  const action = useFormAction();
  const isSubmitting = useIsSubmitting({
    formAction: action,
    formMethod: "PATCH",
  });
  const inputRef = useRef<HTMLInputElement>(null);
  const [previewImage, setPreviewImage] = useState<string | null>(null);

  useEffect(() => {
    if (photo !== previewImage) {
      setPreviewImage(null);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [photo]);

  return (
    <Form
      method="PATCH"
      encType="multipart/form-data"
      className="w-24 h-24 flex justify-center items-center relative mx-auto"
    >
      <img
        src={previewImage ?? photo}
        alt="user avatar"
        className="w-24 h-24 rounded-full border border-slate-200 shadow-sm object-cover"
      />
      {previewImage ? (
        <>
          <button
            type="submit"
            className="absolute -bottom-3 -right-3 w-10 h-10 p-2 bg-indigo-500 disabled:bg-indigo-200 rounded-lg justify-center items-center flex"
            aria-label="update avatar"
            disabled={isSubmitting}
          >
            <CheckIcon pathProps={{ className: "stroke-white" }} />
          </button>
          <button
            type="button"
            onClick={() => {
              setPreviewImage(null);
              if (inputRef.current?.value) {
                inputRef.current.value = "";
              }
            }}
            className="absolute top-0 right-0 w-5 h-5 p-1 bg-red-400 rounded-lg justify-center items-center flex"
          >
            <CrossIcon pathProps={{ className: "stroke-white" }} />
          </button>
        </>
      ) : (
        <button
          type="button"
          onClick={() => {
            inputRef.current?.click();
          }}
          className="absolute -bottom-3 -right-3 w-10 h-10 p-2 bg-indigo-500 rounded-lg justify-center items-center flex"
          aria-label="image picker"
        >
          <PenIcon />
        </button>
      )}
      <input
        ref={inputRef}
        hidden
        onChange={(event) => {
          const file = event.target.files?.[0];

          if (file) {
            const reader = new FileReader();
            reader.onloadend = () => {
              setPreviewImage(reader.result as string);
            };
            reader.readAsDataURL(file);
          } else {
            setPreviewImage(photo);
          }
        }}
        accept="image/*"
        type="file"
        name="avatar"
      />
    </Form>
  );
};

export default PhotoPicker;
