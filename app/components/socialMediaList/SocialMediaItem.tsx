import { MediaTypes, SocialMedia } from "@prisma/client";
import { FC } from "react";

import social1 from "~/assets/images/facebook.png";
import social2 from "~/assets/images/instargam.png";
import social3 from "~/assets/images/linkedln.png";
import social4 from "~/assets/images/skype.png";
import social5 from "~/assets/images/telegram.png";

const socialsSrc = {
  [MediaTypes.Facebook]: social1,
  [MediaTypes.Instagram]: social2,
  [MediaTypes.Linkedin]: social3,
  [MediaTypes.Skype]: social4,
  [MediaTypes.Telegram]: social5,
};

interface SocialMediaItemProps extends SocialMedia {
  className?: string;
}

const SocialMediaItem: FC<SocialMediaItemProps> = ({
  link,
  type,
  className,
}) => {
  return (
    <li className={className}>
      <a href={link} target="_blank" rel="noreferrer">
        <img
          src={socialsSrc[type]}
          alt={`social ${type}`}
          className="w-6 h-6 justify-center items-center inline-flex"
        />
      </a>
    </li>
  );
};

export default SocialMediaItem;
