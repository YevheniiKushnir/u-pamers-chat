import { SocialMedia } from "@prisma/client";
import { FC } from "react";

import { cn } from "~/utils/styles";

import SocialMediaItem from "./SocialMediaItem";

interface SocialMediaListProps {
  socialMedias: SocialMedia[];
  className?: string;
}

const SocialMediaList: FC<SocialMediaListProps> = ({
  socialMedias,
  className,
}) => {
  if (!socialMedias.length) return null;

  return (
    <ul
      className={cn(
        "flex gap-3 pt-3 items-center",
        className ? { [className]: Boolean(className) } : {},
      )}
    >
      {socialMedias.map((s) => (
        <SocialMediaItem key={s.id} {...s} />
      ))}
    </ul>
  );
};

export default SocialMediaList;
