import { User } from "@prisma/client";
import { Link, NavLink, NavLinkProps } from "@remix-run/react";
import { FC } from "react";

import MessageIcon from "~/assets/icons/MessageIcon";
import logoImg from "~/assets/images/logo.png";

interface HeaderProps extends Pick<User, "photo"> {
  isAuth: boolean;
  isUserHasMessages: boolean;
}

const activeLink: NavLinkProps["className"] = ({ isActive }) =>
  isActive
    ? "bg-indigo-500/60 px-2 h-full flex items-center justify-center border-b-4 border-indigo-500"
    : "px-2 h-full flex items-center justify-center";

const Header: FC<HeaderProps> = ({ isAuth, photo, isUserHasMessages }) => {
  return (
    <header className="px-2 bg-indigo-900 sticky">
      <div className="container max-w-5xl mx-auto h-14 px-0.5 py-0.5 justify-between items-center flex">
        <Link to="/">
          <img src={logoImg} alt="u-pamers" />
        </Link>
        {isAuth && photo ? (
          <div className="flex gap-3 items-center h-full">
            <NavLink to="/chats" className={activeLink}>
              <div className="relative">
                <MessageIcon />
                {isUserHasMessages ? (
                  <span className="absolute -top-0.5 -right-0.5 w-2 h-2 rounded-3xl bg-red-500 shadow shadow-red-400" />
                ) : null}
              </div>
            </NavLink>
            <NavLink to="/profile" className={activeLink}>
              <img
                src={photo}
                alt="user avatar"
                className="w-7 h-7 rounded-full border border-slate-200"
              />
            </NavLink>
          </div>
        ) : (
          <Link
            to="signin"
            className="px-4 py-2.5 bg-indigo-500 text-white rounded-lg justify-center items-center flex"
          >
            Log in
          </Link>
        )}
      </div>
    </header>
  );
};

export default Header;
