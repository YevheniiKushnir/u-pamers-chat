import BannerTemplate, { BannerLink } from "./BannerTemplate";

const SingInBanner = () => {
  return (
    <BannerTemplate title="Wanna connect with U-pamers?">
      Start your journey with <BannerLink to="/signin">login</BannerLink>.
    </BannerTemplate>
  );
};

export default SingInBanner;
