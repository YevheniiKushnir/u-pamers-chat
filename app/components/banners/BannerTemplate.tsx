import { Link, LinkProps } from "@remix-run/react";
import { FC, ReactNode, useState } from "react";

import removeImg from "~/assets/images/cross.png";
import infoImg from "~/assets/images/info.png";
import { cn } from "~/utils/styles";

interface BannerTemplateProps {
  title: string;
  children: ReactNode;
}

export const BannerLink: FC<LinkProps> = ({
  children,
  className,
  ...props
}) => (
  <Link
    className={cn(
      "text-sky-600 text-sm font-medium leading-tight",
      className
        ? {
            [className]: Boolean(className),
          }
        : {},
    )}
    {...props}
  >
    {children}
  </Link>
);

const BannerTemplate: FC<BannerTemplateProps> = ({ children, title }) => {
  const [showBanner, setShowBanner] = useState(true);

  if (!showBanner) return null;

  return (
    <div className="p-5 bg-white justify-evenly items-start gap-3 grid grid-cols-[1fr_auto] w-full border-b-2 border-sky-600">
      <div className="flex items-start gap-2">
        <img src={infoImg} alt="info" />
        <div>
          <h3 className="text-black text-base font-semibold leading-tight">
            {title}
          </h3>
          <p className="text-gray-500 text-sm font-medium leading-tight">
            {children}
          </p>
        </div>
      </div>
      <button onClick={() => setShowBanner(!showBanner)} className="w-6 h-6">
        <img src={removeImg} alt="remove" />
      </button>
    </div>
  );
};

export default BannerTemplate;
