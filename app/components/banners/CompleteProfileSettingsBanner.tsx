import BannerTemplate, { BannerLink } from "./BannerTemplate";

const CompleteProfileSettingsBanner = () => {
  return (
    <BannerTemplate title="Wanna stand out?">
      Help others connect with you faster by{" "}
      <BannerLink to="/settings/profile">
        adding more profile details
      </BannerLink>
      .
    </BannerTemplate>
  );
};

export default CompleteProfileSettingsBanner;
