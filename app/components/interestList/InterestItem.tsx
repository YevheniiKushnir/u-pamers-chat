import { FC, HTMLAttributes, ReactNode } from "react";

interface InterestItemProps extends HTMLAttributes<HTMLDivElement> {
  icon: ReactNode;
  text: string;
  children?: ReactNode;
}

const InterestItem: FC<InterestItemProps> = ({
  icon,
  text,
  children,
  className,
  ...props
}) => {
  return (
    <div
      className={`bg-indigo-50 rounded-3xl border border-indigo-500 justify-start items-center gap-2 flex py-1 px-1 pr-4 ${className}`}
      {...props}
    >
      <div className="bg-indigo-900 rounded-full p-2">{icon}</div>
      <p className="text-black text-base font-medium leading-normal">{text}</p>
      {children}
    </div>
  );
};

export default InterestItem;
