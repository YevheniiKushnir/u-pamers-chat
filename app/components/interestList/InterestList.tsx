import { InterestTypes, Interests } from "@prisma/client";
import { Link } from "@remix-run/react";
import { FC, ReactNode } from "react";

import BarCharIcon from "~/assets/icons/BarCharIcon";
import BasketbalIcon from "~/assets/icons/BasketbalIcon";
import BookIcon from "~/assets/icons/BookIcon";
import BowlIcon from "~/assets/icons/BowlIcon";
import BriefcaseIcon from "~/assets/icons/BriefcaseIcon";
import BugIcon from "~/assets/icons/BugIcon";
import BuildingIcon from "~/assets/icons/BuildingIcon";
import FamilyIcon from "~/assets/icons/FamilyIcon";
import PlaneIcon from "~/assets/icons/PlaneIcon";

import InterestItem from "./InterestItem";

export const interestsAssets = [
  {
    icon: <PlaneIcon />,
    type: InterestTypes.Travel,
    text: "Travel",
  },
  {
    icon: <BasketbalIcon />,
    type: InterestTypes.Sports,
    text: "Sports",
  },
  {
    icon: <BarCharIcon />,
    type: InterestTypes.Promotions,
    text: "Promotions",
  },
  {
    icon: <BriefcaseIcon />,
    type: InterestTypes.Office,
    text: "Office",
  },
  {
    icon: <FamilyIcon />,
    type: InterestTypes.Family,
    text: "Family",
  },
  {
    icon: <BookIcon />,
    type: InterestTypes.Education,
    text: "Education",
  },
  {
    icon: <BowlIcon />,
    type: InterestTypes.Dining,
    text: "Dining",
  },
  {
    icon: <BuildingIcon />,
    type: InterestTypes.Attractions,
    text: "Attractions",
  },
  {
    icon: <BugIcon />,
    type: InterestTypes.Health,
    text: "Health",
  },
];

interface InterestListProps {
  interests?: Interests[];
  children?: ReactNode;
}

const InterestList: FC<InterestListProps> = ({ interests, children }) => {
  return (
    <ul className="flex flex-wrap gap-3 pt-4 items-center">
      {children ? (
        children
      ) : interests?.length ? (
        interests.map((interest) => {
          const asstet = interestsAssets.find(
            ({ type }) => type === interest.type,
          )!;

          return (
            <li key={interest.id}>
              <InterestItem icon={asstet.icon} text={asstet.text} />
            </li>
          );
        })
      ) : (
        <p className="mt-4 text-gray-500 text-sm font-medium leading-tight">
          You have not selected any interests yet.{" "}
          <Link
            to="/settings/profile?edit=interests"
            className="text-sky-600 text-sm font-medium leading-tight"
          >
            Edit your interests.
          </Link>
        </p>
      )}
    </ul>
  );
};

export default InterestList;
