import { Link } from "@remix-run/react";

import EmptyBox from "~/assets/icons/EmptyBox";

const ChatEmptyList = () => {
  return (
    <div className="flex flex-col items-center justify-center h-full px-4 py-6">
      <EmptyBox />
      <div className="mt-6">
        <h2 className="text-center text-black text-2xl font-bold leading-loose">
          Chats is empty
        </h2>
        <p className="mt-1 text-center text-gray-500 text-base font-medium leading-normal">
          You don’t have any chat yet. Open a profile catalog page and find your
          firs online friend!
        </p>
      </div>
      <Link
        to="/"
        className="mt-8 w-full px-12 py-4 bg-indigo-500 rounded-lg justify-center items-center inline-flex text-center text-white text-base font-semibold leading-tight"
      >
        Catalog
      </Link>
    </div>
  );
};

export default ChatEmptyList;
