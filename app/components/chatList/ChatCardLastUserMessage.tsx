import { FC } from "react";

interface ChatCardLastUserMessageProps {
  message: string | undefined;
  isTyping: boolean;
}

const ChatCardLastUserMessage: FC<ChatCardLastUserMessageProps> = ({
  isTyping,
  message,
}) => {
  if (isTyping) {
    return <p className="text-gray-500 text-xs leading-tight">is typing...</p>;
  }

  return (
    <p className="text-gray-500 text-sm font-medium leading-tight whitespace-nowrap text-ellipsis w-52 overflow-hidden">
      {message ? message : "No messages yet"}
    </p>
  );
};

export default ChatCardLastUserMessage;
