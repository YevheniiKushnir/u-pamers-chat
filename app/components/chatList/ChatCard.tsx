import { Link } from "@remix-run/react";
import { FC } from "react";

import { UserWithTyping } from "~/types";
import { gerMessageTime } from "~/utils/time";

import ChatCardLastUserMessage from "./ChatCardLastUserMessage";

interface ChatCardProps {
  user: UserWithTyping;
}

const ChatCard: FC<ChatCardProps> = ({ user }) => {
  const chat = user.chats[0];

  return (
    <Link
      to={chat.id}
      className="rounded px-1 group hover:bg-slate-100 active:bg-slate-100"
    >
      <div className="flex items-center gap-4">
        {user.photo ? (
          <img
            src={user.photo}
            alt="user avatar"
            className="w-12 h-12 rounded-full border border-slate-200 object-cover"
          />
        ) : (
          <div className="w-12 h-12 rounded-full border border-slate-200" />
        )}
        <div className="flex py-4 gap-9 justify-between flex-1 border-b-2 border-b-zinc-200 group-hover:border-b-transparent group-active:border-b-transparent">
          <div>
            <h4 className="text-black text-base font-semibold leading-tight mb-1">
              {user.firstName} {user.lastName}
            </h4>
            <ChatCardLastUserMessage
              isTyping={user.isTyping}
              message={chat?.messages.at(0)?.message}
            />
          </div>
          <div className="flex flex-col items-end gap-2">
            {chat?.messages.at(0)?.createdAt ? (
              <p className="text-gray-500 text-xs leading-none">
                {gerMessageTime(chat?.messages.at(0)?.createdAt)}
              </p>
            ) : null}
            {chat?._count?.messages ? (
              <span className="w-5 h-5 px-0.5 bg-red-500 rounded-full justify-center items-center flex text-white text-xs font-bold leading-3">
                {chat._count.messages}
              </span>
            ) : null}
          </div>
        </div>
      </div>
    </Link>
  );
};

export default ChatCard;
