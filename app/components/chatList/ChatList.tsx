import { FC } from "react";

import { UserWithTyping } from "~/types";

import ChatCard from "./ChatCard";

interface ChatListProps {
  users: UserWithTyping[];
}

const ChatList: FC<ChatListProps> = ({ users }) => {
  return (
    <div className="flex flex-col">
      {users.map((u) => (
        <ChatCard key={u.id} user={u} />
      ))}
    </div>
  );
};

export default ChatList;
