import { Interests, Privacy, Profile, SocialMedia, User } from "@prisma/client";

import { prisma } from "~/db.server";

export async function getProfileByUserId(userId: User["id"]) {
  return prisma.profile.findUnique({ where: { userId } });
}

export async function getProfileWithInterestsSocialMediasByUserId(
  userId: User["id"],
) {
  return prisma.profile.findUnique({
    where: { userId },
    select: {
      about: true,
      location: true,
      birthdate: true,
      interests: true,
      socialMedias: true,
    },
  });
}

export async function getProfilePrivacy(userId: User["id"]) {
  return prisma.profile.findUnique({
    where: { userId },
    select: { privacy: true },
  });
}

export async function updateProfile(
  profileId: Profile["id"],
  interests: Omit<Interests, "id">[],
  socialMedia: Omit<SocialMedia, "id">[],
  generalInfo: Partial<Profile>,
) {
  return prisma.$transaction([
    prisma.interests.deleteMany({
      where: {
        profileId,
      },
    }),
    prisma.socialMedia.deleteMany({
      where: {
        profileId,
      },
    }),
    prisma.interests.createMany({
      data: interests,
    }),
    prisma.socialMedia.createMany({
      data: socialMedia,
    }),
    prisma.profile.update({
      where: {
        id: profileId,
      },
      data: generalInfo,
    }),
  ]);
}

export async function updatePrivacy(
  profileId: Profile["id"],
  privacy: Partial<Privacy>,
) {
  return prisma.privacy.update({
    where: {
      profileId,
    },
    data: privacy,
  });
}
