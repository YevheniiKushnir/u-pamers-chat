import { Chat, Message, User } from "@prisma/client";

import { prisma } from "~/db.server";

export async function createUsersChat(userIds: User["id"][]) {
  return prisma.chat.create({
    data: {
      users: {
        connect: [{ id: userIds[0] }, { id: userIds[1] }],
      },
    },
  });
}

export async function getUserUnreadChatsCount(userId: User["id"]) {
  return prisma.chat.count({
    where: {
      users: {
        some: {
          id: {
            equals: userId,
          },
        },
      },
      messages: {
        some: {
          isSended: {
            equals: true,
          },
          isReaded: {
            equals: false,
          },
          NOT: {
            userId: {
              equals: userId,
            },
          },
        },
      },
    },
  });
}

export async function getUserUnreadedMessages(
  userId: User["id"],
  chatId: Chat["id"],
) {
  return prisma.message.findMany({
    where: {
      chatId: {
        equals: chatId,
      },
      isSended: {
        equals: true,
      },
      isReaded: {
        equals: false,
      },
      NOT: {
        userId: {
          equals: userId,
        },
      },
    },
    select: {
      id: true,
    },
  });
}

export async function getUserChat(userId: User["id"], chatId: Chat["id"]) {
  return prisma.chat.findFirst({
    where: {
      id: {
        equals: chatId,
      },
      users: {
        some: {
          id: {
            equals: userId,
          },
        },
      },
    },
    select: {
      users: true,
      id: true,
      messages: {
        take: 100,
        orderBy: {
          createdAt: "asc",
        },
      },
      _count: {
        select: {
          messages: true,
        },
      },
    },
  });
}

export async function sendMessageInChat(
  userId: User["id"],
  chatId: Chat["id"],
  message: Message["message"],
) {
  return prisma.message.create({
    data: {
      message,
      chatId,
      userId,
      isSended: true,
    },
  });
}

export async function readMessage(messageId: Message["id"]) {
  return prisma.message.update({
    where: {
      id: messageId,
    },
    data: {
      isReaded: true,
    },
  });
}

export async function readMessages(messageIds: Message["id"][]) {
  return prisma.message.updateMany({
    where: {
      id: {
        in: messageIds,
      },
    },
    data: {
      isReaded: true,
    },
  });
}

export async function verifyChatExisting(usersIds: User["id"][]) {
  const chats = await prisma.chat.findMany({
    where: {
      users: {
        every: {
          id: {
            in: usersIds,
          },
        },
      },
    },
  });

  if (chats.length) {
    return chats[0].id;
  }

  return false;
}
