import { Connection, User } from "@prisma/client";

import { prisma } from "~/db.server";
import { ConnectionDataType } from "~/types";

import { getUserUnreadChatsCount } from "./chat.server";

export const createConnectionRecord = async (
  selfConnectionId: ConnectionDataType["selfConnectionId"],
  userId: User["id"],
): Promise<string> => {
  const connection = await prisma.connection.create({
    data: {
      connectionId: selfConnectionId,
      userId,
    },
  });

  return connection.id;
};

export const verifyConnection = async (
  selfConnectionId: ConnectionDataType["selfConnectionId"],
  userId: User["id"],
): Promise<boolean> => {
  const connection = await prisma.connection.findFirst({
    where: {
      connectionId: selfConnectionId,
      userId,
    },
  });

  return Boolean(connection);
};

export const verifyConnectionRequest = async (
  selfConnectionId: ConnectionDataType["selfConnectionId"],
  connectionId: Connection["connectionId"],
) => {
  const connection = await prisma.connection.findFirst({
    where: {
      id: selfConnectionId,
      connectionId: connectionId!,
    },
    select: {
      userId: true,
    },
  });

  return connection?.userId;
};

export const getConnectionUserData = async (userId: User["id"]) => {
  const user = await prisma.user.findFirst({
    where: {
      id: userId,
    },
  });

  const unreadChatsCount = await getUserUnreadChatsCount(userId);

  return {
    userName: `${user?.firstName}, ${user?.lastName}`,
    unreadChatsCount,
  };
};
