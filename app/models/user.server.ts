import type { Password, SocialMedia, User } from "@prisma/client";
import bcrypt from "bcryptjs";

import { prisma } from "~/db.server";

export type { User } from "@prisma/client";

export async function getUserById(id: User["id"]) {
  return prisma.user.findUnique({ where: { id } });
}

export async function getUserByEmail(email: User["email"]) {
  return prisma.user.findUnique({ where: { email } });
}

export async function createUser(
  user: Pick<User, "email" | "firstName" | "lastName">,
  password: string,
) {
  const hashedPassword = await bcrypt.hash(password, 10);

  return prisma.user.create({
    data: {
      ...user,
      password: {
        create: {
          hash: hashedPassword,
        },
      },
      profile: {
        create: {
          privacy: {
            create: {},
          },
        },
      },
    },
  });
}

export async function deleteUserByEmail(email: User["email"]) {
  return prisma.user.delete({ where: { email } });
}

export async function verifySignin(
  email: User["email"],
  password: Password["hash"],
) {
  const userWithPassword = await prisma.user.findUnique({
    where: { email },
    include: {
      password: true,
    },
  });

  if (!userWithPassword || !userWithPassword.password) {
    return null;
  }

  const isValid = await bcrypt.compare(
    password,
    userWithPassword.password.hash,
  );

  if (!isValid) {
    return null;
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const { password: _password, ...userWithoutPassword } = userWithPassword;

  return userWithoutPassword;
}

export async function updateUserById(
  userId: User["id"],
  userData: Partial<Omit<User, "id">>,
) {
  return prisma.user.update({
    where: { id: userId },
    data: {
      ...userData,
    },
  });
}

export async function updateUserSocials(
  userId: string,
  socials: Pick<SocialMedia, "link" | "type" | "profileId">[],
) {
  const data = await prisma.$transaction([
    prisma.socialMedia.deleteMany({
      where: {
        profile: {
          userId: {
            equals: userId,
          },
        },
      },
    }),
    prisma.socialMedia.createMany({ data: socials }),
  ]);

  return data[1];
}

export async function getUsers(userId: User["id"], search?: string) {
  return prisma.user.findMany({
    select: {
      photo: true,
      firstName: true,
      lastName: true,
      id: true,
      profile: {
        select: {
          socialMedias: true,
        },
      },
    },
    orderBy: {
      createdAt: "asc",
    },
    where: {
      NOT: {
        id: {
          equals: userId,
        },
      },
      OR: [
        { firstName: { contains: search } },
        { lastName: { contains: search } },
      ],
    },
  });
}

export async function getUsersWithChats(userId: User["id"], search?: string) {
  return prisma.user.findMany({
    where: {
      NOT: {
        id: {
          equals: userId,
        },
      },
      OR: [
        { firstName: { contains: search } },
        { lastName: { contains: search } },
      ],
      chats: {
        some: {
          users: {
            some: {
              id: userId,
            },
          },
        },
      },
    },
    select: {
      photo: true,
      id: true,
      firstName: true,
      lastName: true,
      chats: {
        where: {
          users: {
            some: {
              id: userId,
            },
          },
        },
        select: {
          id: true,
          messages: {
            orderBy: {
              createdAt: "desc",
            },
            take: 1,
            select: {
              message: true,
              createdAt: true,
              userId: true,
              id: true,
            },
          },
          _count: {
            select: {
              messages: {
                where: {
                  isSended: {
                    equals: true,
                  },
                  isReaded: {
                    equals: false,
                  },
                  NOT: {
                    userId: {
                      equals: userId,
                    },
                  },
                },
              },
            },
          },
        },
        take: 1,
      },
    },
  });
}

export async function getUserWithSocials(userId: User["id"]) {
  return prisma.user.findUnique({
    where: {
      id: userId,
    },
    include: {
      profile: {
        select: {
          socialMedias: true,
        },
      },
    },
  });
}

export async function getUserProfile(userId: User["id"]) {
  return prisma.user.findUnique({
    where: {
      id: userId,
    },
    select: {
      profile: true,
    },
  });
}

export async function getUserWithProfile(userId: User["id"]) {
  return prisma.user.findUnique({
    where: {
      id: userId,
    },
    select: {
      firstName: true,
      lastName: true,
      photo: true,
      profile: {
        include: {
          socialMedias: true,
          interests: true,
        },
      },
    },
  });
}

export async function updateUserPassword(userId: User["id"], password: string) {
  const hashedPassword = await bcrypt.hash(password, 10);

  return prisma.password.update({
    where: {
      userId,
    },
    data: {
      hash: hashedPassword,
    },
  });
}

export async function deleteUserById(userId: User["id"]) {
  return prisma.user.delete({
    where: {
      id: userId,
    },
  });
}
