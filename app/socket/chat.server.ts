import { Chat, Message, User } from "@prisma/client";
import { Server, Socket } from "socket.io";

import { readMessage, sendMessageInChat } from "~/models/chat.server";
import { ClientToServerEvents, ServerToClientEvents } from "~/types";

export default (
  io: Server<ClientToServerEvents, ServerToClientEvents>,
  socket: Socket<ClientToServerEvents, ServerToClientEvents>,
) => {
  const joinToRoom = (chatId: Chat["id"]) => {
    socket.join(chatId);
  };

  const sendMessage = async (
    chatId: Chat["id"],
    data: Pick<Message, "userId" | "message" | "chatId">,
    cb: () => void,
  ) => {
    const message = await sendMessageInChat(
      data.userId,
      data.chatId,
      data.message,
    );

    io.to(chatId).emit("chat:update:message", message);
    cb();
  };

  const userTyping =
    (typing: boolean) =>
    (
      chatId: Chat["id"],
      userId: User["id"],
      roomUserId: User["id"],
      cb?: () => void,
    ) => {
      io.to(chatId).emit("chat:update:typing", userId, typing);
      io.to(roomUserId).emit("user:update:typing", userId, typing);
      if (cb) {
        cb();
      }
    };

  const userReadMessage = async (
    chatId: Chat["id"],
    messageId: Message["id"],
  ) => {
    const message = await readMessage(messageId);

    io.to(chatId).emit("chat:update:message:read", message);
  };

  socket.on("chat:join", joinToRoom);
  socket.on("chat:message", sendMessage);
  socket.on("chat:message:read", userReadMessage);
  socket.on("chat:start:typing", userTyping(true));
  socket.on("chat:end:typing", userTyping(false));
};
