import { Server, Socket } from "socket.io";

import chatServer from "./chat.server";
import userServer from "./user.server";

export const socketConnection = (io: Server, socket: Socket) => {
  chatServer(io, socket);
  userServer(io, socket);
};
