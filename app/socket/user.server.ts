import { User } from "@prisma/client";
import { Server, Socket } from "socket.io";

import { getUsersWithChats } from "~/models/user.server";
import { ClientToServerEvents, ServerToClientEvents } from "~/types";

export default (
  io: Server<ClientToServerEvents, ServerToClientEvents>,
  socket: Socket<ClientToServerEvents, ServerToClientEvents>,
) => {
  const userJoin = (userId: User["id"]) => {
    socket.join(userId);
  };

  const receiveMessage = async (userId: User["id"]) => {
    const users = await getUsersWithChats(userId, "");

    io.to(userId).emit("user:update:message", users);
  };

  socket.on("user:join", userJoin);
  socket.on("user:message", receiveMessage);
};
