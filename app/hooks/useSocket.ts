import { useState, useEffect } from "react";
import { io } from "socket.io-client";

import { ClientSocketType } from "~/types";

const useSocket = () => {
  const [socket, setSocket] = useState<ClientSocketType | undefined>(undefined);

  useEffect(() => {
    if (!window) return;

    const newSocket = io(window.location.origin);

    setSocket(newSocket);

    return () => {
      newSocket.close();
    };
  }, []);

  return socket;
};

export default useSocket;
