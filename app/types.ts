import { Chat, Message, User } from "@prisma/client";
import { Socket } from "socket.io-client";

import { getUsers, getUsersWithChats } from "./models/user.server";

export type UserWithSocials = Awaited<ReturnType<typeof getUsers>>[number];

export type UserWithChats = Awaited<
  ReturnType<typeof getUsersWithChats>
>[number];

export interface GroupedByUserMessages {
  userId: string;
  isSended: boolean;
  isReaded: boolean;
  createdAt: Date;
  messages: Message[];
}

export type GroupedByDateMessages<T = Message> = readonly [string, T[]];

export type UserWithTyping = UserWithChats & { isTyping: boolean };

export interface ServerToClientEvents {
  "chat:update:typing": (userId: User["id"], typing: boolean) => void;
  "chat:update:message": (message: Message) => void;
  "chat:update:message:read": (message: Message) => void;
  "user:update:typing": (userId: User["id"], typing: boolean) => void;
  "user:update:message": (users: UserWithChats[]) => void;
}

export interface ClientToServerEvents {
  "chat:join": (chatId: Chat["id"]) => void;
  "chat:message": (
    chatId: Chat["id"],
    data: Pick<Message, "userId" | "message" | "chatId">,
    cb: () => void,
  ) => void;
  "chat:message:read": (chatId: Chat["id"], messageId: Message["id"]) => void;
  "chat:start:typing": (
    chatId: Chat["id"],
    userId: User["id"],
    roomUserId: User["id"],
  ) => void;
  "chat:end:typing": (
    chatId: Chat["id"],
    userId: User["id"],
    roomUserId: User["id"],
    cb?: () => void,
  ) => void;
  "user:join": (userId: User["id"]) => void;
  "user:message": (userId: User["id"]) => void;
}

export type ClientSocketType = Socket<
  ServerToClientEvents,
  ClientToServerEvents
>;

export interface ConnectionDataType {
  userName: string;
  selfConnectionId: string;
  selfRedirectUrl: string;
  selfName: string;
}
