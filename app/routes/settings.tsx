import { LoaderFunctionArgs, json, redirect } from "@remix-run/node";
import { Outlet, useLoaderData } from "@remix-run/react";

import Header from "~/components/header/Header";
import { getUserUnreadChatsCount } from "~/models/chat.server";
import { getUser } from "~/utils/session.server";

export const loader = async ({ request }: LoaderFunctionArgs) => {
  const user = await getUser(request);
  if (!user) return redirect("/signin");

  const unreadChatsCount = await getUserUnreadChatsCount(user.id);

  return json({
    user,
    isAuth: Boolean(user),
    isUserHasMessages: Boolean(unreadChatsCount),
  });
};

const Settings = () => {
  const loaderData = useLoaderData<typeof loader>();

  return (
    <>
      <Header
        isAuth={loaderData.isAuth}
        photo={loaderData.user.photo}
        isUserHasMessages={loaderData.isUserHasMessages}
      />
      <main className="bg-zinc-100 min-h-full p-4">
        <div className="container mx-auto max-w-4xl">
          <Outlet />
        </div>
      </main>
    </>
  );
};

export default Settings;
