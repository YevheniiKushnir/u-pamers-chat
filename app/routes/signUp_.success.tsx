import { DataFunctionArgs, json, redirect } from "@remix-run/node";
import { Link } from "@remix-run/react";

import closeImg from "~/assets/images/cross.png";
import successImg from "~/assets/images/success.png";
import { requireUserId } from "~/utils/session.server";

export const loader = async ({ request }: DataFunctionArgs) => {
  const url = new URL(request.url);
  const prevPage = url.searchParams.get("prevPage");

  if (prevPage !== "/signup/socials") {
    return redirect("/signup/socials");
  }

  await requireUserId(request, "/signup");

  return json({});
};

const SignUpSeccessPage = () => {
  return (
    <>
      <header className="container mx-auto p-4 flex items-center justify-center relative">
        <h4 className="text-black text-base font-semibold leading-tight">
          Sing up
        </h4>
        <button className="absolute right-3">
          <img className="h-6 w-6" src={closeImg} alt="leave page" />
        </button>
      </header>
      <main className="container mx-auto px-4 py-6 flex flex-col items-center justify-center h-full">
        <img src={successImg} alt="success signup" />
        <h2 className="mt-6 text-center text-black text-2xl font-bold leading-loose">
          Account created
        </h2>
        <p className="mt-2 text-center text-gray-500 text-base font-medium leading-normal">
          Fill in your profile details to introduce yourself to other U-pamers.
        </p>
        <div className="flex flex-col gap-3 mt-8 md:flex-row max-md:w-full">
          <Link
            to="/settings/profile"
            className="px-12 py-4 bg-indigo-500 rounded-lg text-center text-white text-base font-semibold leading-tight"
          >
            Fill in profile details
          </Link>
          <Link
            to="/"
            className="px-12 py-4 rounded-lg border border-indigo-500 text-center text-indigo-500 text-base font-semibold leading-tight"
          >
            Go to the Catalog
          </Link>
        </div>
      </main>
    </>
  );
};

export default SignUpSeccessPage;
