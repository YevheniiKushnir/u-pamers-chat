import { Link, NavLink, NavLinkProps, Outlet } from "@remix-run/react";

import closeImg from "~/assets/images/cross.png";

const activeLink: NavLinkProps["className"] = ({ isActive }) =>
  isActive
    ? "w-10 h-10 p-2 bg-white rounded-full border-2 border-indigo-500 justify-center items-center flex text-indigo-500 text-base font-medium leading-normal"
    : "w-10 h-10 p-2 bg-white rounded-full border border-gray-400 flex-col justify-center items-center flex text-gray-400 text-base font-medium leading-normal";

const SignUp = () => {
  return (
    <>
      <header className="container mx-auto p-4 flex items-center justify-center relative">
        <h4 className="text-black text-base font-semibold leading-tight">
          Sing up
        </h4>
        <Link to="/signUp/abort?fromPage=/signup" className="absolute right-3">
          <img className="h-6 w-6" src={closeImg} alt="leave page" />
        </Link>
      </header>
      <main className="container mx-auto px-4 py-6">
        <section className="flex items-center gap-1">
          <NavLink to="/signup" className={activeLink} end>
            1
          </NavLink>
          <span className="h-px px-1 bg-gray-400 flex-1"></span>
          <NavLink to="/signup/photo" className={activeLink}>
            2
          </NavLink>
          <span className="h-px px-1 bg-gray-400 flex-1"></span>
          <NavLink to="/signup/socials" className={activeLink}>
            3
          </NavLink>
        </section>
        <section className="container mt-6 max-w-sm mx-auto">
          <Outlet />
          <p className="mt-4 text-center text-neutral-600 text-base font-medium leading-normal">
            Already have an account?{" "}
            <Link
              to="/signin"
              className="text-indigo-500 text-base font-medium leading-none"
            >
              Login
            </Link>
          </p>
        </section>
      </main>
    </>
  );
};

export default SignUp;
