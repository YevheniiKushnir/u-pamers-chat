import { conform, useFieldset, useForm } from "@conform-to/react";
import { getFieldsetConstraint, parse } from "@conform-to/zod";
import { type ActionFunctionArgs, json, redirect } from "@remix-run/node";
import { Form, useActionData } from "@remix-run/react";
import { useEffect } from "react";

import SettingsGroup from "~/components/pages/settings/SettingsGroup";
import SettingsHeader from "~/components/pages/settings/SettingsHeader";
import SettingsSubmitButton from "~/components/pages/settings/SettingsSubmitButton";
import InputField from "~/components/ui/input/Input";
import { updateUserPassword, verifySignin } from "~/models/user.server";
import { SettingsPasswordSchema } from "~/utils/schema";
import { getUser } from "~/utils/session.server";

export const action = async ({ request }: ActionFunctionArgs) => {
  const user = await getUser(request);

  if (!user) return redirect("/signin");

  const formData = await request.formData();

  const submission = parse(formData, {
    schema: SettingsPasswordSchema,
  });

  if (submission.intent !== "submit") {
    return json({ status: "idle", submission } as const);
  }

  if (!submission.value) {
    return json({ status: "error", submission } as const, {
      status: 400,
    });
  }

  try {
    const {
      currentPassword,
      password: { password },
    } = submission.value;
    const verifiedUser = await verifySignin(user.email, currentPassword);

    if (!verifiedUser) {
      return json({ status: "error", submission } as const, {
        status: 403,
      });
    }

    await updateUserPassword(user.id, password);

    return json({ status: "success", submission });
  } catch (error) {
    throw new Response("Error while updating user password", { status: 500 });
  }
};

const SettingPassword = () => {
  const actionData = useActionData<typeof action>();

  const [form, fields] = useForm({
    id: "sign-up-user",
    constraint: getFieldsetConstraint(SettingsPasswordSchema),
    lastSubmission: actionData?.submission,
    onValidate({ formData }) {
      return parse(formData, { schema: SettingsPasswordSchema });
    },
  });
  const { confirm, password } = useFieldset(form.ref, fields.password);

  useEffect(() => {
    if (actionData?.status === "success") {
      form.ref.current?.reset();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [actionData]);

  return (
    <>
      <SettingsHeader routeTo="/settings" routeName="Settings">
        Change password
      </SettingsHeader>
      <div className="mt-4">
        <SettingsGroup>
          <Form className="flex flex-col gap-4" method="POST" {...form.props}>
            <button type="submit" className="hidden" />
            <InputField
              id={fields.currentPassword.id}
              label="Current password"
              errors={
                fields.currentPassword.errors
                  ? fields.currentPassword.errors
                  : actionData?.status === "error"
                  ? ["Password uncorrect"]
                  : undefined
              }
              {...conform.input(fields.currentPassword)}
              type="password"
              required={false}
            />
            <div>
              <InputField
                id={password.id}
                label="New password"
                errors={password.errors}
                {...conform.input(password)}
                type="password"
              />
              <p className="mt-1 text-zinc-800 text-xs font-normal leading-none">
                Create a strong password that is at least 8 characters long,
                includes upper-case, lower-case letters, at least 1 digit and 1
                special character.
              </p>
            </div>
            <InputField
              id={confirm.id}
              label="Confirm new password"
              errors={confirm.errors}
              {...conform.input(confirm)}
              type="password"
            />
            <SettingsSubmitButton />
          </Form>
        </SettingsGroup>
      </div>
    </>
  );
};

export default SettingPassword;
