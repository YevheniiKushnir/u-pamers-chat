import { conform, useForm } from "@conform-to/react";
import { getFieldsetConstraint, parse } from "@conform-to/zod";
import { type ActionFunctionArgs, json } from "@remix-run/node";
import { Form, Link, useActionData } from "@remix-run/react";

import closeImg from "~/assets/images/cross.png";
import logoBigImg from "~/assets/images/logo-big.png";
import InputField, { ErrorList } from "~/components/ui/input/Input";
import useIsSubmitting from "~/hooks/useIsSubmitting";
import { verifySignin } from "~/models/user.server";
import { UserSigninCredSchema } from "~/utils/schema";
import { createUserSession } from "~/utils/session.server";

export const action = async ({ request }: ActionFunctionArgs) => {
  const formData = await request.formData();

  const submission = parse(formData, {
    schema: UserSigninCredSchema,
  });

  if (submission.intent !== "submit") {
    return json({
      status: "idle",
      submission,
      errors: [] as string[],
    } as const);
  }

  if (!submission.value) {
    return json(
      { status: "error", submission, errors: [] as string[] } as const,
      {
        status: 400,
      },
    );
  }

  const { email, password } = submission.value;

  try {
    const user = await verifySignin(email, password);

    if (!user) {
      return json(
        {
          status: "error",
          submission,
          errors: ["User credetials are unvalid"],
        },
        {
          status: 401,
        },
      );
    }

    return createUserSession({
      redirectTo: "/",
      remember: true,
      request,
      userId: user.id,
    });
  } catch (error) {
    throw new Response("Error while signin user", { status: 500 });
  }
};

const SignIn = () => {
  const isSubmitting = useIsSubmitting();
  const actionData = useActionData<typeof action>();

  const [form, fields] = useForm({
    id: "sign-in-user",
    constraint: getFieldsetConstraint(UserSigninCredSchema),
    lastSubmission: actionData?.submission,
    onValidate({ formData }) {
      return parse(formData, { schema: UserSigninCredSchema });
    },
  });

  return (
    <>
      <header className="container mx-auto p-4 flex items-center justify-center relative">
        <h4 className="text-black text-base font-semibold leading-tight">
          Sing in
        </h4>
        <button className="absolute right-3">
          <img className="h-6 w-6" src={closeImg} alt="leave page" />
        </button>
      </header>
      <main className="container mx-auto px-4 py-6">
        <img src={logoBigImg} alt="logo" className="w-24 h-24 mx-auto" />
        <h4 className="mt-4 text-center text-black text-3xl font-bold leading-10">
          U-pamers
        </h4>
        <p className="text-center text-gray-500 text-base font-medium leading-normal">
          Log in to expand your social network and communicate with colleagues.
        </p>
        <Form
          method="POST"
          className="mt-7 flex flex-col gap-4 max-w-sm mx-auto"
          {...form.props}
        >
          <InputField label="Email" {...conform.input(fields.email)} />
          <InputField
            label="Password"
            {...conform.input(fields.password)}
            type="password"
          />
          <ErrorList
            id={form.id}
            errors={actionData?.errors ? actionData.errors : []}
          />
          <button
            type="submit"
            className="w-full mt-8 px-12 py-4 bg-indigo-500 rounded-lg justify-center items-center inline-flex text-center text-white text-base font-semibold leading-tight disabled:bg-gray-100 disabled:text-gray-400"
            disabled={isSubmitting}
          >
            Rock and roll!
          </button>
        </Form>
        <p className="mt-4 text-center text-neutral-600 text-base font-medium leading-normal">
          Don’t have an account?{" "}
          <Link
            to="/signup"
            className="text-indigo-500 text-base font-medium leading-none"
          >
            Sign up
          </Link>
        </p>
      </main>
    </>
  );
};

export default SignIn;
