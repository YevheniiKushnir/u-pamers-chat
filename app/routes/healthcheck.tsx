import { json } from "@remix-run/node";

import { prisma } from "~/db.server";

export const loader = async () => {
  try {
    await Promise.all([prisma.user.count()]);
    return json("OK", { status: 200 });
  } catch (error: unknown) {
    console.log("healthcheck ❌", { error });
    return json("ERROR", { status: 500 });
  }
};
