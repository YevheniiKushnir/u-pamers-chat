import { conform, useFieldset, useForm } from "@conform-to/react";
import { getFieldsetConstraint, parse } from "@conform-to/zod";
import { InterestTypes, MediaTypes } from "@prisma/client";
import {
  type ActionFunctionArgs,
  type LoaderFunctionArgs,
  json,
} from "@remix-run/node";
import { Form, useActionData, useLoaderData } from "@remix-run/react";
import invariant from "tiny-invariant";

import CrossIcon from "~/assets/icons/CrossIcon";
import facebookImg from "~/assets/images/facebook.png";
import instargamImg from "~/assets/images/instargam.png";
import linkedlnImg from "~/assets/images/linkedln.png";
import skypeImg from "~/assets/images/skype.png";
import telegramImg from "~/assets/images/telegram.png";
import InterestItem from "~/components/interestList/InterestItem";
import InterestList, {
  interestsAssets,
} from "~/components/interestList/InterestList";
import SettingsGroup from "~/components/pages/settings/SettingsGroup";
import SettingsHeader from "~/components/pages/settings/SettingsHeader";
import SettingsSubmitButton from "~/components/pages/settings/SettingsSubmitButton";
import InputField from "~/components/ui/input/Input";
import {
  getProfileByUserId,
  getProfileWithInterestsSocialMediasByUserId,
  updateProfile,
} from "~/models/profile.server";
import {
  transformGeneralInfoObjectDTO,
  transformInterestsObjectDTO,
  transformSocialsObjectDTO,
} from "~/utils/common";
import { SettingsProfileDetailsSchema } from "~/utils/schema";
import { requireUserId } from "~/utils/session.server";

export const loader = async ({ request }: LoaderFunctionArgs) => {
  const userId = await requireUserId(request);

  const profile = await getProfileWithInterestsSocialMediasByUserId(userId);

  invariant(profile, `We can't find user profile by user id:${userId}`);

  return json({
    profile: {
      ...profile,
      interests: profile.interests.reduce(
        (acc, { type }) => {
          acc[type] = "on";
          return acc;
        },
        {} as Record<InterestTypes, string>,
      ),
      socialMedias: profile.socialMedias.reduce(
        (acc, { type, link }) => {
          acc[type] = link;
          return acc;
        },
        {} as Record<MediaTypes, string>,
      ),
    },
  });
};

export const action = async ({ request }: ActionFunctionArgs) => {
  const userId = await requireUserId(request);

  const formData = await request.formData();

  const submission = parse(formData, {
    schema: SettingsProfileDetailsSchema,
  });

  if (submission.intent !== "submit") {
    return json({ status: "idle", submission } as const);
  }

  if (!submission.value) {
    return json({ status: "error", submission } as const, {
      status: 400,
    });
  }

  const profile = await getProfileByUserId(userId);

  invariant(profile, `We can't find user profile by user id:${userId}`);

  try {
    const { generalInfo, interests, socialMedia } = submission.value;

    await updateProfile(
      profile.id,
      transformInterestsObjectDTO(interests, profile.id),
      transformSocialsObjectDTO(socialMedia, profile.id),
      transformGeneralInfoObjectDTO(generalInfo),
    );

    return json({ status: "success", submission });
  } catch (error) {
    throw new Response("Error while updating user password", { status: 500 });
  }
};

const SettingProfile = () => {
  const loaderData = useLoaderData<typeof loader>();
  const actionData = useActionData<typeof action>();

  const [form, fields] = useForm({
    id: "settings-profile-details",
    constraint: getFieldsetConstraint(SettingsProfileDetailsSchema),
    lastSubmission: actionData?.submission,
    onValidate({ formData }) {
      return parse(formData, { schema: SettingsProfileDetailsSchema });
    },
    defaultValue: {
      generalInfo: {
        about: loaderData.profile?.about ?? undefined,
        birthdate: loaderData.profile?.birthdate ?? undefined,
        location: loaderData.profile?.location ?? undefined,
      },
      interests: loaderData.profile.interests,
      socialMedia: loaderData.profile.socialMedias,
    },
  });
  const generalInfoFields = useFieldset(form.ref, fields.generalInfo);
  const interestsFields = useFieldset(form.ref, fields.interests);
  const socialMediaFields = useFieldset(form.ref, fields.socialMedia);

  return (
    <>
      <SettingsHeader routeTo="/settings" routeName="Settings">
        Profile details
      </SettingsHeader>
      <div className="py-4">
        <Form method="POST" className="flex flex-col gap-4" {...form.props}>
          <button className="hidden" type="submit" />
          <SettingsGroup groupName="General Info">
            <InputField
              label="Date of birth"
              {...conform.input(generalInfoFields.birthdate)}
              name={generalInfoFields.birthdate.name}
              defaultValue={loaderData.profile?.birthdate ?? undefined}
              errors={generalInfoFields.birthdate.errors}
              type="date"
              required
            />
            <InputField
              label="Location"
              {...conform.input(generalInfoFields.location)}
              errors={generalInfoFields.location.errors}
              required
            />
            <InputField
              label="About me"
              {...conform.input(generalInfoFields.about)}
              errors={generalInfoFields.location.errors}
              className="h-5"
              required
            />
          </SettingsGroup>
          <SettingsGroup groupName="Social media">
            <div className="flex gap-3 items-center [&>div]:flex-1">
              <img src={linkedlnImg} alt="Linkedln" className="w-6 h-6" />
              <InputField
                label="Linkedln"
                {...conform.input(socialMediaFields.Linkedin)}
              />
            </div>
            <div className="flex gap-3 items-center [&>div]:flex-1">
              <img src={instargamImg} alt="Instargam" className="w-6 h-6" />
              <InputField
                label="Instargam"
                {...conform.input(socialMediaFields.Instagram)}
              />
            </div>
            <div className="flex gap-3 items-center [&>div]:flex-1">
              <img src={telegramImg} alt="Telegram" className="w-6 h-6" />
              <InputField
                label="Telegram"
                {...conform.input(socialMediaFields.Telegram)}
              />
            </div>
            <div className="flex gap-3 items-center [&>div]:flex-1">
              <img src={facebookImg} alt="Facebook" className="w-6 h-6" />
              <InputField
                label="Facebook"
                {...conform.input(socialMediaFields.Facebook)}
              />
            </div>
            <div className="flex gap-3 items-center [&>div]:flex-1">
              <img src={skypeImg} alt="Skype" className="w-6 h-6" />
              <InputField
                label="Skype"
                {...conform.input(socialMediaFields.Skype)}
              />
            </div>
          </SettingsGroup>
          <SettingsGroup groupName="Interests">
            <InterestList>
              {interestsAssets.map((source) => (
                <InterestItem
                  key={`settings-profile-${source.text}`}
                  className="has-[:checked]:bg-indigo-50 bg-white"
                  {...source}
                >
                  <label
                    htmlFor={interestsFields[source.type].id}
                    aria-label={`switch toggle ${source.type}`}
                    className="cursor-pointer"
                  >
                    {/* eslint-disable-next-line jsx-a11y/label-has-associated-control */}
                    <input
                      id={interestsFields[source.type].id}
                      {...conform.input(interestsFields[source.type], {
                        hidden: true,
                        type: "checkbox",
                      })}
                      className="peer"
                      defaultChecked={Boolean(
                        loaderData.profile.interests[source.type],
                      )}
                    />
                    <CrossIcon
                      svgProps={{
                        className:
                          "transition peer-[:checked]:rotate-0 peer-[&:not(:checked)]:rotate-45",
                      }}
                    />
                  </label>
                </InterestItem>
              ))}
            </InterestList>
          </SettingsGroup>
          <SettingsSubmitButton />
        </Form>
      </div>
    </>
  );
};

export default SettingProfile;
