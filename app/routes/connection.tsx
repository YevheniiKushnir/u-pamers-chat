import { type LoaderFunctionArgs, json } from "@remix-run/node";
import { useLoaderData } from "@remix-run/react";
import { useEffect } from "react";

import {
  createConnectionRecord,
  verifyConnection,
} from "~/models/connection.server";
import {
  createConnectionRedirectUrl,
  getConnectionDataFromQuery,
  validateConnectionOrigin,
} from "~/utils/connection.server";
import { getUserId } from "~/utils/session.server";

export const loader = async ({ request }: LoaderFunctionArgs) => {
  const userId = await getUserId(request);

  if (!userId) {
    return json({
      status: "warning",
      message:
        "User not found, please sign in or sign up and after try again to connect",
    } as const);
  }

  const searchParams = new URL(request.url).searchParams;
  const queryData = getConnectionDataFromQuery(searchParams);

  if (!queryData) {
    return json({
      status: "warning",
      message: "Failed to parse data, please go back and try again",
    } as const);
  }

  const connectionUrl = new URL(queryData.selfRedirectUrl);
  const validConnectionOrigin = validateConnectionOrigin(connectionUrl.origin);

  if (!validConnectionOrigin) {
    return json({
      status: "failed",
      message: "Not allowed action",
    } as const);
  }

  const connectionCreated = await verifyConnection(
    queryData.selfConnectionId,
    userId,
  );

  const connectionId = connectionCreated
    ? undefined
    : await createConnectionRecord(queryData.selfConnectionId, userId);

  const redirectUrl = createConnectionRedirectUrl(
    queryData.selfRedirectUrl,
    connectionId,
  );

  return json({
    status: "success",
    message: `Connection successfully. Redirection back on ${queryData.selfName}`,
    payload: {
      selfName: queryData.selfName,
      userName: queryData.userName,
      url: redirectUrl,
    },
  } as const);
};

const Connection = () => {
  const loaderData = useLoaderData<typeof loader>();

  useEffect(() => {
    if (loaderData.status === "success") {
      setTimeout(() => {
        window.open(loaderData.payload.url, "_blank");
      }, 5_000);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <main className="p-4 bg-zinc-100 min-h-full">
      {loaderData.status === "warning" ? (
        <div className="flex p-4 mb-4 rounded-xl bg-amber-50" role="alert">
          <div className="mr-2">
            <svg
              className="flex-shrink-0 w-5 h-5"
              viewBox="0 0 20 20"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M10.0043 13.3333V9.16663M9.99984 6.66663H10.0073M9.99984 18.3333C5.39746 18.3333 1.6665 14.6023 1.6665 9.99996C1.6665 5.39759 5.39746 1.66663 9.99984 1.66663C14.6022 1.66663 18.3332 5.39759 18.3332 9.99996C18.3332 14.6023 14.6022 18.3333 9.99984 18.3333Z"
                stroke="#F59E0B"
                strokeWidth="1.5"
                strokeLinecap="round"
                strokeLinejoin="round"
              />
            </svg>
          </div>
          <div className="block">
            <h3 className="text-amber-500 font-normal text-base">
              <span className="font-semibold mr-1">Warning</span> Your action is
              not allowed
            </h3>
            <p className="mt-1 text-gray-600 text-sm">{loaderData.message}</p>
          </div>
        </div>
      ) : null}
      {loaderData.status === "success" ? (
        <div
          className="flex p-4 mb-4 rounded-xl bg-emerald-50 border border-emerald-400"
          role="alert"
        >
          <div className="mr-2">
            <svg
              className="flex-shrink-0 w-5 h-5"
              viewBox="0 0 20 20"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M10.0043 13.3333V9.16663M9.99984 6.66663H10.0073M9.99984 18.3333C5.39746 18.3333 1.6665 14.6023 1.6665 9.99996C1.6665 5.39759 5.39746 1.66663 9.99984 1.66663C14.6022 1.66663 18.3332 5.39759 18.3332 9.99996C18.3332 14.6023 14.6022 18.3333 9.99984 18.3333Z"
                stroke="#10B981"
                strokeWidth="1.5"
                strokeLinecap="round"
                strokeLinejoin="round"
              ></path>
            </svg>
          </div>
          <div className="block">
            <h3 className="text-emerald-500 font-normal text-base">
              <span className="font-semibold mr-1">Success</span>Your,{" "}
              {loaderData.payload.userName}, connection to{" "}
              {loaderData.payload.selfName} is successful
            </h3>
            <p className="mt-1 text-gray-600 text-sm">{loaderData.message}</p>
          </div>
        </div>
      ) : null}
      {loaderData.status === "failed" ? (
        <div className="flex p-4 mb-4 rounded-xl bg-red-500" role="alert">
          <div className="mr-2">
            <svg
              className="flex-shrink-0 w-5 h-5"
              viewBox="0 0 20 20"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M10.0043 13.3333V9.16663M9.99984 6.66663H10.0073M9.99984 18.3333C5.39746 18.3333 1.6665 14.6023 1.6665 9.99996C1.6665 5.39759 5.39746 1.66663 9.99984 1.66663C14.6022 1.66663 18.3332 5.39759 18.3332 9.99996C18.3332 14.6023 14.6022 18.3333 9.99984 18.3333Z"
                stroke="#fff"
                strokeWidth="1.5"
                strokeLinecap="round"
                strokeLinejoin="round"
              ></path>
            </svg>
          </div>
          <div className="block">
            <h3 className="text-white font-normal text-base">
              <span className="font-semibold mr-1">Error</span>Your actions is
              not allowed
            </h3>
            <p className="mt-1 text-white/80 text-sm">{loaderData.message}</p>
          </div>
        </div>
      ) : null}
    </main>
  );
};

export default Connection;
