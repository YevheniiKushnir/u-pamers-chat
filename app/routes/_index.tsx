import {
  type LoaderFunctionArgs,
  type MetaFunction,
  type ActionFunctionArgs,
  redirect,
  json,
} from "@remix-run/node";
import { useLoaderData } from "@remix-run/react";
import invariant from "tiny-invariant";

import SingInBanner from "~/components/banners/SingInBanner";
import Filter from "~/components/filter/Filter";
import Header from "~/components/header/Header";
import ProfileEmptyList from "~/components/profileList/ProfileEmptyList";
import ProfileList from "~/components/profileList/ProfileList";
import { SEARCH_KEY } from "~/constants";
import {
  createUsersChat,
  getUserUnreadChatsCount,
  verifyChatExisting,
} from "~/models/chat.server";
import { getUserById, getUsers } from "~/models/user.server";
import { safeRedirect } from "~/utils";
import { getURLSearchParams } from "~/utils/common";
import { getUser } from "~/utils/session.server";

export const meta: MetaFunction = () => [{ title: "U-PAMERS | Kushnir" }];

export const loader = async ({ request }: LoaderFunctionArgs) => {
  const user = await getUser(request);
  if (!user) return redirect("/signin");

  const search = getURLSearchParams(request.url).get(SEARCH_KEY) ?? "";

  const [users, unreadChatsCount] = await Promise.all([
    getUsers(user.id, search),
    getUserUnreadChatsCount(user.id),
  ]);

  return json({
    user,
    users,
    isAuth: Boolean(user),
    isSearching: Boolean(search.length),
    isUserHasMessages: Boolean(unreadChatsCount),
  });
};

export const action = async ({ request }: ActionFunctionArgs) => {
  const formData = await request.formData();

  const userId = String(formData.get("userId"));
  invariant(userId, "User id should be provided");

  const newUser = await getUserById(userId);
  invariant(newUser, `User with id:${userId} doesn't exist`);

  const user = await getUser(request);
  if (!user) return redirect(safeRedirect("/signin"));

  const chatId = await verifyChatExisting([user.id, newUser.id]);

  if (chatId) {
    return redirect(safeRedirect(`/chats/${chatId}`));
  }

  const createdChat = await createUsersChat([user.id, newUser.id]);

  return redirect(safeRedirect(`/chats/${createdChat.id}`));
};

export default function Index() {
  const loaderData = useLoaderData<typeof loader>();

  return (
    <>
      <Header
        isAuth={loaderData.isAuth}
        photo={loaderData.user.photo}
        isUserHasMessages={loaderData.isUserHasMessages}
      />
      {loaderData.users.length || loaderData.isSearching ? (
        <>
          {loaderData.isAuth ? null : <SingInBanner />}
          <main className="bg-zinc-100 h-full">
            <Filter />
            <section className="container max-w-4xl mx-auto">
              <ProfileList users={loaderData.users} />
            </section>
          </main>
        </>
      ) : (
        <ProfileEmptyList />
      )}
    </>
  );
}
