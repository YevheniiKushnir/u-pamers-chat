import SettingsHeader from "~/components/pages/settings/SettingsHeader";
import SettingsLinkPage from "~/components/pages/settings/SettingsLinkPage";

const SettingIndex = () => {
  return (
    <>
      <SettingsHeader routeTo="/" routeName="Catalogue">
        Settings
      </SettingsHeader>
      <section className="mt-4 flex flex-col gap-2">
        <SettingsLinkPage routeTo="account">Account details</SettingsLinkPage>
        <SettingsLinkPage routeTo="password">Change password</SettingsLinkPage>
        <SettingsLinkPage routeTo="profile">Profile details</SettingsLinkPage>
        <SettingsLinkPage routeTo="privacy">Privacy</SettingsLinkPage>
      </section>
    </>
  );
};

export default SettingIndex;
