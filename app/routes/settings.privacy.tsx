import { conform, useForm } from "@conform-to/react";
import { getFieldsetConstraint, parse } from "@conform-to/zod";
import {
  type ActionFunctionArgs,
  type LoaderFunctionArgs,
  json,
} from "@remix-run/node";
import { Form, useActionData, useLoaderData } from "@remix-run/react";
import { useEffect, useState } from "react";
import invariant from "tiny-invariant";

import SettingsGroup from "~/components/pages/settings/SettingsGroup";
import SettingsHeader from "~/components/pages/settings/SettingsHeader";
import SettingsSubmitButton from "~/components/pages/settings/SettingsSubmitButton";
import SettingsToggleFiled from "~/components/pages/settings/SettingsToggleFiled";
import ToggleInput from "~/components/ui/input/ToggleInput";
import {
  getProfileByUserId,
  getProfilePrivacy,
  updatePrivacy,
} from "~/models/profile.server";
import { transformPrivacyObjectDTO } from "~/utils/common";
import { SettingsPrivacySchema } from "~/utils/schema";
import { requireUserId } from "~/utils/session.server";
import { cn } from "~/utils/styles";

export const loader = async ({ request }: LoaderFunctionArgs) => {
  const userId = await requireUserId(request);

  const profile = await getProfilePrivacy(userId);

  invariant(profile, `We can't find user profile by user id:${userId}`);

  return json({
    privacy: {
      accountPrivate: profile.privacy?.accountPrivate ? "on" : "off",
      showAge: profile.privacy?.showAge ? "on" : "off",
      showLocation: profile.privacy?.showLocation ? "on" : "off",
      showDescription: profile.privacy?.showDescription ? "on" : "off",
    },
  });
};

export const action = async ({ request }: ActionFunctionArgs) => {
  const userId = await requireUserId(request);

  const formData = await request.formData();

  const submission = parse(formData, {
    schema: SettingsPrivacySchema,
  });

  if (submission.intent !== "submit") {
    return json({ status: "idle", submission } as const);
  }

  if (!submission.value) {
    return json({ status: "error", submission } as const, {
      status: 400,
    });
  }

  const profile = await getProfileByUserId(userId);

  invariant(profile, `We can't find user profile by user id:${userId}`);

  try {
    const userPrivacy = transformPrivacyObjectDTO(submission.value);

    await updatePrivacy(profile.id, userPrivacy);

    return json({ submission });
  } catch (error) {
    throw new Response("Error while updating user Privacy", { status: 500 });
  }
};

const SettingPrivacy = () => {
  const loaderData = useLoaderData<typeof loader>();
  const actionData = useActionData<typeof action>();
  const [accounPrivate, setAccounPrivate] = useState(
    loaderData.privacy.accountPrivate === "on" ? true : false,
  );

  const [form, fields] = useForm({
    id: "settings-privacy",
    constraint: getFieldsetConstraint(SettingsPrivacySchema),
    lastSubmission: actionData?.submission,
    onValidate({ formData }) {
      return parse(formData, { schema: SettingsPrivacySchema });
    },
    defaultValue: loaderData.privacy,
  });

  useEffect(() => {
    setAccounPrivate(loaderData.privacy.accountPrivate === "on" ? true : false);
  }, [loaderData.privacy]);

  return (
    <>
      <SettingsHeader routeTo="/settings" routeName="Settings">
        Privacy
      </SettingsHeader>
      <div className="mt-4">
        <SettingsGroup>
          <Form method="PATCH" className="flex flex-col gap-4" {...form.props}>
            <div className="flex gap-2 items-top justify-between">
              <div>
                <p className="text-neutral-950 text-base font-semibold leading-tight">
                  Private Account
                </p>
                <p className="mt-2 text-zinc-800 text-sm font-light leading-tight">
                  The private account is not viewable to users who are not
                  logged in
                </p>
              </div>
              <ToggleInput
                handleChange={(value) => setAccounPrivate(value)}
                {...conform.input(fields.accountPrivate, { type: "checkbox" })}
                name="accountPrivate"
                defaultChecked={accounPrivate}
              />
            </div>
            <div
              className={cn(
                "flex flex-col gap-4 transition-opacity ease-linear",
                {
                  "opacity-30": accounPrivate,
                },
              )}
            >
              <SettingsToggleFiled text="Age">
                <ToggleInput
                  disabled={accounPrivate}
                  {...conform.input(fields.showAge, { type: "checkbox" })}
                  name="showAge"
                  defaultChecked={
                    fields.showAge.defaultValue === "on" ? true : false
                  }
                />
              </SettingsToggleFiled>
              <SettingsToggleFiled text="Location">
                <ToggleInput
                  disabled={accounPrivate}
                  {...conform.input(fields.showLocation, { type: "checkbox" })}
                  name="showLocation"
                  defaultChecked={
                    fields.showLocation.defaultValue === "on" ? true : false
                  }
                />
              </SettingsToggleFiled>
              <SettingsToggleFiled text="Description">
                <ToggleInput
                  disabled={accounPrivate}
                  {...conform.input(fields.showDescription, {
                    type: "checkbox",
                  })}
                  name="showDescription"
                  defaultChecked={
                    fields.showDescription.defaultValue === "on" ? true : false
                  }
                />
              </SettingsToggleFiled>
            </div>
            <SettingsSubmitButton />
          </Form>
        </SettingsGroup>
      </div>
    </>
  );
};

export default SettingPrivacy;
