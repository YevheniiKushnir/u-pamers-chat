import { LoaderFunctionArgs, json, redirect } from "@remix-run/node";
import { useLoaderData, useRevalidator } from "@remix-run/react";
import { useEffect, useState } from "react";

import ChatEmptyList from "~/components/chatList/ChatEmptyList";
import ChatList from "~/components/chatList/ChatList";
import Header from "~/components/header/Header";
import InputSearch from "~/components/ui/inputSearch/InputSearch";
import { SEARCH_KEY } from "~/constants";
import useSocket from "~/hooks/useSocket";
import { getUserUnreadChatsCount } from "~/models/chat.server";
import { getUsersWithChats } from "~/models/user.server";
import { UserWithTyping } from "~/types";
import { getUser } from "~/utils/session.server";

export const loader = async ({ request }: LoaderFunctionArgs) => {
  const user = await getUser(request);
  if (!user) return redirect("/signin");

  const url = new URL(request.url);
  const search = url.searchParams.get(SEARCH_KEY) ?? "";

  const [users, unreadChatsCount] = await Promise.all([
    getUsersWithChats(user.id, search),
    getUserUnreadChatsCount(user.id),
  ]);

  return json({
    user,
    users,
    isAuth: Boolean(user),
    isUserHasMessages: Boolean(unreadChatsCount),
    isSearching: Boolean(search.length),
  });
};

const Chats = () => {
  const loaderData = useLoaderData<typeof loader>();
  const revalidator = useRevalidator();
  const socket = useSocket();

  const [users, setUsers] = useState<UserWithTyping[]>(
    loaderData.users as unknown as UserWithTyping[],
  );

  useEffect(() => {
    if (!socket) return;

    socket.on("connect", () => {
      socket.emit("user:join", loaderData.user.id);
    });

    socket.on("user:update:typing", (userId, typing) => {
      setUsers((prev) =>
        prev.map((u) =>
          u.id === userId
            ? {
                ...u,
                isTyping: typing,
              }
            : u,
        ),
      );
    });

    socket.on("user:update:message", (updatedUsers) => {
      setUsers((prev) => {
        if (
          updatedUsers.every(({ id }) => Boolean(prev.find((u) => u.id === id)))
        ) {
          return prev.map((u) => ({
            ...u,
            ...(updatedUsers.find(({ id }) => id === u.id) ?? {}),
          }));
        }

        return updatedUsers.map((u) => ({
          ...u,
          isTyping: false,
          ...(prev.find(({ id }) => id === u.id) ?? {}),
        }));
      });

      revalidator.revalidate();
    });

    return () => {
      socket.off("connect");
      socket.off("user:update:message");
      socket.off("user:update:typing");
    };
  }, [loaderData.user.id, socket, revalidator]);

  return (
    <>
      <Header
        isAuth={loaderData.isAuth}
        photo={loaderData.user.photo}
        isUserHasMessages={loaderData.isUserHasMessages}
      />
      <main className="container max-w-4xl mx-auto py-6 px-4">
        <h2 className="text-zinc-800 text-2xl font-bold leading-loose">
          Messages
        </h2>
        <InputSearch sx="my-4" />
        {loaderData.users.length || loaderData.isSearching ? (
          <ChatList users={users} />
        ) : (
          <ChatEmptyList />
        )}
      </main>
    </>
  );
};

export default Chats;
