import { conform, useForm } from "@conform-to/react";
import { getFieldsetConstraint, parse } from "@conform-to/zod";
import {
  type ActionFunctionArgs,
  type LoaderFunctionArgs,
  json,
  redirect,
} from "@remix-run/node";
import { Form, useActionData, useLoaderData } from "@remix-run/react";

import SettingsGroup from "~/components/pages/settings/SettingsGroup";
import SettingsHeader from "~/components/pages/settings/SettingsHeader";
import SettingsSubmitButton from "~/components/pages/settings/SettingsSubmitButton";
import InputField from "~/components/ui/input/Input";
import { updateUserById } from "~/models/user.server";
import { SettingsAccountSchema } from "~/utils/schema";
import { getUser, requireUserId } from "~/utils/session.server";

export const loader = async ({ request }: LoaderFunctionArgs) => {
  const user = await getUser(request);

  if (!user) return redirect("/signin");

  return json({
    user: {
      firstName: user.firstName,
      lastName: user.lastName,
      email: user.email,
    },
  });
};

export const action = async ({ request }: ActionFunctionArgs) => {
  const userId = await requireUserId(request);

  const formData = await request.formData();

  const submission = parse(formData, {
    schema: SettingsAccountSchema,
  });

  if (submission.intent !== "submit") {
    return json({ status: "idle", submission } as const);
  }

  if (!submission.value) {
    return json({ status: "error", submission } as const, {
      status: 400,
    });
  }

  try {
    const userData = submission.value;
    await updateUserById(userId, userData);

    return json({ submission });
  } catch (error) {
    throw new Response("Error while creating user", { status: 500 });
  }
};

const SettingAccount = () => {
  const actionData = useActionData<typeof action>();
  const loaderData = useLoaderData<typeof loader>();

  const [form, fields] = useForm({
    id: "sign-up-user",
    constraint: getFieldsetConstraint(SettingsAccountSchema),
    lastSubmission: actionData?.submission,
    onValidate({ formData }) {
      return parse(formData, { schema: SettingsAccountSchema });
    },
    defaultValue: {
      email: loaderData.user.email,
      firstName: loaderData.user.firstName,
      lastName: loaderData.user.lastName,
    },
  });

  return (
    <>
      <SettingsHeader routeTo="/settings" routeName="Settings">
        Account details
      </SettingsHeader>
      <div className="mt-4">
        <SettingsGroup>
          <Form className="flex flex-col gap-4" method="POST" {...form.props}>
            <button type="submit" className="hidden" />
            <InputField
              id={fields.email.id}
              label="Email"
              errors={fields.email.errors}
              {...conform.input(fields.email)}
            />
            <InputField
              id={fields.firstName.id}
              label="First name"
              errors={fields.firstName.errors}
              {...conform.input(fields.firstName)}
              type="text"
            />
            <InputField
              id={fields.lastName.id}
              label="Last name"
              errors={fields.lastName.errors}
              {...conform.input(fields.lastName)}
              type="text"
            />
            <SettingsSubmitButton />
          </Form>
        </SettingsGroup>
      </div>
    </>
  );
};

export default SettingAccount;
