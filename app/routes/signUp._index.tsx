import { useForm, conform, useFieldset } from "@conform-to/react";
import { getFieldsetConstraint, parse } from "@conform-to/zod";
import { type ActionFunctionArgs, json } from "@remix-run/node";
import { Form, useActionData } from "@remix-run/react";

import InputField from "~/components/ui/input/Input";
import useIsSubmitting from "~/hooks/useIsSubmitting";
import { createUser } from "~/models/user.server";
import { UserCredSchema } from "~/utils/schema";
import { createUserSession } from "~/utils/session.server";

export const action = async ({ request }: ActionFunctionArgs) => {
  const formData = await request.formData();

  const submission = parse(formData, {
    schema: UserCredSchema,
  });

  if (submission.intent !== "submit") {
    return json({ status: "idle", submission } as const);
  }

  if (!submission.value) {
    return json({ status: "error", submission } as const, {
      status: 400,
    });
  }
  const {
    password: { password },
    ...user
  } = submission.value;
  try {
    const { id } = await createUser(user, password);

    return createUserSession({
      redirectTo: "/signUp/photo?prevPage=/signup",
      remember: true,
      request,
      userId: id,
    });
  } catch (error) {
    throw new Response("Error while creating user", { status: 500 });
  }
};

const SignUpIndex = () => {
  const isSubmitting = useIsSubmitting();
  const actionData = useActionData<typeof action>();

  const [form, fields] = useForm({
    id: "sign-up-user",
    constraint: getFieldsetConstraint(UserCredSchema),
    lastSubmission: actionData?.submission,
    onValidate({ formData }) {
      return parse(formData, { schema: UserCredSchema });
    },
  });
  const { confirm, password } = useFieldset(form.ref, fields.password);

  return (
    <>
      <h5 className="text-center text-black text-2xl font-bold leading-loose">
        Fill in account details
      </h5>
      <Form method="POST" {...form.props} className="mt-6">
        <button type="submit" className="hidden" />
        <div className="flex flex-col gap-4">
          <InputField
            id={fields.email.id}
            label="Email"
            errors={fields.email.errors}
            {...conform.input(fields.email)}
          />
          <InputField
            id={fields.firstName.id}
            label="First name"
            errors={fields.firstName.errors}
            {...conform.input(fields.firstName)}
            type="text"
          />
          <InputField
            id={fields.lastName.id}
            label="Last name"
            errors={fields.lastName.errors}
            {...conform.input(fields.lastName)}
            type="text"
          />
          <div>
            <InputField
              id={password.id}
              label="Password"
              errors={password.errors}
              {...conform.input(password)}
              type="password"
              required
            />
            <p className="mt-1 text-zinc-800 text-xs font-normal leading-none">
              Create a strong password that is at least 8 characters long,
              includes upper-case, lower-case letters, at least 1 digit and 1
              special character.
            </p>
          </div>
          <InputField
            id={confirm.id}
            label="Confirm password"
            errors={confirm.errors}
            {...conform.input(confirm)}
            type="password"
            required
          />
        </div>
        <button
          type="submit"
          className="w-full mt-8 px-12 py-4 bg-indigo-500 rounded-lg justify-center items-center inline-flex text-center text-white text-base font-semibold leading-tight disabled:bg-gray-100 disabled:text-gray-400"
          disabled={isSubmitting}
        >
          Continue
        </button>
      </Form>
    </>
  );
};

export default SignUpIndex;
