import { type LoaderFunctionArgs, json } from "@remix-run/node";

import {
  getConnectionUserData,
  verifyConnectionRequest,
} from "~/models/connection.server";
import {
  allowedConnections,
  getConnectionUserDataFromQuery,
} from "~/utils/connection.server";

export const loader = async ({ request }: LoaderFunctionArgs) => {
  const searchParams = new URL(request.url).searchParams;
  const connectionData = getConnectionUserDataFromQuery(searchParams);

  if (!connectionData) {
    return json({
      error: "Denied",
    });
  }

  const userId = await verifyConnectionRequest(
    connectionData?.selfConnectionId,
    connectionData?.connectionId,
  );

  if (!userId) {
    return json({
      error: "Failed to load data",
    });
  }

  const data = await getConnectionUserData(userId);

  return json(
    { data },
    {
      headers: {
        "Access-Control-Allow-Origin": allowedConnections.toString(),
      },
    },
  );
};
