import { parse } from "@conform-to/zod";
import {
  type LoaderFunctionArgs,
  type ActionFunctionArgs,
  json,
  unstable_parseMultipartFormData as parseMultipartFormData,
  unstable_createMemoryUploadHandler as createMemoryUploadHandler,
  unstable_composeUploadHandlers as composeUploadHandlers,
} from "@remix-run/node";
import { Link, useLoaderData } from "@remix-run/react";
import { UploadApiResponse } from "cloudinary";
import invariant from "tiny-invariant";

import ArrowLeftIcon from "~/assets/icons/ArrowLeftIcon";
import EmptyBox from "~/assets/icons/EmptyBox";
import CompleteProfileSettingsBanner from "~/components/banners/CompleteProfileSettingsBanner";
import Header from "~/components/header/Header";
import PhotoPicker from "~/components/pages/profile/PhotoPicker";
import ProfileAbout from "~/components/pages/profile/ProfileAbout";
import ProfileInterests from "~/components/pages/profile/ProfileInterests";
import SocialMediaList from "~/components/socialMediaList/SocialMediaList";
import { getUserUnreadChatsCount } from "~/models/chat.server";
import { getUserWithProfile, updateUserById } from "~/models/user.server";
import { uploadImage } from "~/utils/cloudinary.server";
import { ImageSchema, MAX_UPLOAD_SIZE } from "~/utils/schema";
import { getUserId, requireUserId } from "~/utils/session.server";

export const loader = async ({ request }: LoaderFunctionArgs) => {
  const userId = await getUserId(request);
  invariant(userId, "User id not found");

  const [user, unreadChatsCount] = await Promise.all([
    getUserWithProfile(userId),
    getUserUnreadChatsCount(userId),
  ]);
  invariant(user, "User not found");

  return json({
    user,
    isAuth: Boolean(user),
    isUserHasMessages: Boolean(unreadChatsCount),
  });
};

export const action = async ({ request }: ActionFunctionArgs) => {
  const userId = await requireUserId(request);

  const uploadHandler = composeUploadHandlers(
    async ({ name, data }) => {
      if (name !== "avatar") {
        return undefined;
      }

      try {
        const uploadedImage = await uploadImage(data);
        return (uploadedImage as UploadApiResponse).secure_url;
      } catch (error) {
        return null;
      }
    },
    createMemoryUploadHandler({
      maxPartSize: MAX_UPLOAD_SIZE,
    }),
  );

  const formData = await parseMultipartFormData(request, uploadHandler);

  const submission = parse(formData, {
    schema: ImageSchema,
  });

  if (submission.intent !== "submit") {
    return json({ status: "idle", submission } as const);
  }

  if (!submission.value) {
    return json({ status: "error", submission } as const, {
      status: 400,
    });
  }
  const { avatar } = submission.value;
  await updateUserById(userId, { photo: avatar as unknown as string });

  return json({ status: "success" } as const);
};

const Profile = () => {
  const loaderData = useLoaderData<typeof loader>();

  return (
    <>
      <Header
        isAuth={loaderData.isAuth}
        photo={loaderData.user.photo}
        isUserHasMessages={loaderData.isUserHasMessages}
      />
      {loaderData.user.profile?.profileFullFielded ? null : (
        <CompleteProfileSettingsBanner />
      )}
      <main className="p-4 bg-zinc-100 min-h-full">
        <div className="container mx-auto max-w-4xl">
          <Link
            to="/"
            className="mb-4 flex gap-1 items-center text-sky-600 text-base font-medium leading-tight"
          >
            <ArrowLeftIcon /> Catalogue
          </Link>
          <div className="mb-4 flex justify-between items-center">
            <h2 className="text-zinc-800 text-2xl font-bold">My profile</h2>
            <div className="flex items-center gap-2">
              <Link
                to="/logout"
                className="text-indigo-500 text-lg font-normal rounded-lg border border-indigo-500 text-center px-2 py-1 hover:bg-indigo-500 hover:text-white"
              >
                Logout
              </Link>
              <Link
                to="/settings"
                className="text-indigo-500 text-lg font-normal rounded-lg border border-indigo-500 text-center px-2 py-1 hover:bg-indigo-500 hover:text-white"
              >
                Settings
              </Link>
            </div>
          </div>
          <div className="p-4 rounded-xl shadow bg-white relative before:block before:h-14 before:absolute before:top-0 before:right-0 before:left-0 before:rounded-t-xl before:bg-indigo-300">
            <section>
              <PhotoPicker photo={loaderData.user.photo} />
              <h4 className="mt-5 text-center text-black text-2xl font-bold">
                {loaderData.user.firstName} {loaderData.user.lastName}
              </h4>
              <p className="mt-1 text-center text-gray-500 text-sm font-medium leading-tight">
                {loaderData.user.profile?.location}
              </p>
              <SocialMediaList
                socialMedias={loaderData.user.profile?.socialMedias ?? []}
                className="justify-center"
              />
              <Link
                to="/settings/profile"
                className="block w-full max-w-lg mt-9 mx-auto px-12 py-2.5 rounded-lg border border-indigo-500 text-center text-indigo-500 text-base font-semibold leading-tight hover:bg-indigo-500 hover:text-white"
              >
                Edit Profile
              </Link>
            </section>
            {loaderData.user.profile?.profileFullFielded ? (
              <section className="mt-8 flex flex-col gap-8">
                <ProfileAbout text={loaderData.user.profile?.about ?? ""} />
                <ProfileInterests
                  interests={loaderData.user.profile?.interests ?? []}
                />
              </section>
            ) : (
              <section className="mt-8 flex flex-col items-center p-5">
                <EmptyBox />
                <h5 className="mt-5 text-black text-lg font-bold leading-snug">
                  No details yet
                </h5>
                <p className="mt-1 text-gray-500 text-base font-medium leading-normal">
                  This user has not provided details
                </p>
              </section>
            )}
          </div>
        </div>
      </main>
    </>
  );
};

export default Profile;
