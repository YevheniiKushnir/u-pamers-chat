import { LoaderFunctionArgs, redirect } from "@remix-run/node";

import { deleteUserById } from "~/models/user.server";
import { getUserId, logout } from "~/utils/session.server";

export const loader = async ({ request }: LoaderFunctionArgs) => {
  const url = new URL(request.url);
  const prevPage = url.searchParams.get("fromPage");

  if (prevPage !== "/signup") {
    return redirect("/signup");
  }

  const userId = await getUserId(request);

  if (!userId) return redirect("/preview");

  await deleteUserById(userId);

  return logout(request);
};
