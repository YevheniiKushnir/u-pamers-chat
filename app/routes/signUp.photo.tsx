import { conform, useForm } from "@conform-to/react";
import { getFieldsetConstraint, parse } from "@conform-to/zod";
import {
  json,
  redirect,
  unstable_parseMultipartFormData as parseMultipartFormData,
  unstable_createMemoryUploadHandler as createMemoryUploadHandler,
  unstable_composeUploadHandlers as composeUploadHandlers,
  type LoaderFunctionArgs,
  type ActionFunctionArgs,
} from "@remix-run/node";
import { Form, useActionData } from "@remix-run/react";
import { UploadApiResponse } from "cloudinary";
import { useState, useRef } from "react";

import CameraIcon from "~/assets/icons/CameraIcon";
import CrossIcon from "~/assets/icons/CrossIcon";
import PenIcon from "~/assets/icons/PenIcon";
import useIsSubmitting from "~/hooks/useIsSubmitting";
import { updateUserById } from "~/models/user.server";
import { uploadImage } from "~/utils/cloudinary.server";
import { ImageSchema, MAX_UPLOAD_SIZE } from "~/utils/schema";
import { requireUserId } from "~/utils/session.server";

export const loader = async ({ request }: LoaderFunctionArgs) => {
  const url = new URL(request.url);
  const prevPage = url.searchParams.get("prevPage");

  if (prevPage !== "/signup") {
    return redirect("/signup");
  }

  await requireUserId(request, "/signup");

  return json({});
};

export const action = async ({ request }: ActionFunctionArgs) => {
  const userId = await requireUserId(request);

  const uploadHandler = composeUploadHandlers(
    async ({ name, data }) => {
      if (name !== "avatar") {
        return undefined;
      }

      try {
        const uploadedImage = await uploadImage(data);
        return (uploadedImage as UploadApiResponse).secure_url;
      } catch (error) {
        return null;
      }
    },
    createMemoryUploadHandler({
      maxPartSize: MAX_UPLOAD_SIZE,
    }),
  );

  const formData = await parseMultipartFormData(request, uploadHandler);

  const submission = parse(formData, {
    schema: ImageSchema,
  });

  if (submission.intent !== "submit") {
    return json({ status: "idle", submission });
  }

  if (!submission.value) {
    return json({ status: "error", submission } as const, {
      status: 400,
    });
  }
  const { avatar } = submission.value;
  await updateUserById(userId, { photo: avatar as unknown as string });

  return redirect("/signup/socials?prevPage=/signup/photo");
};

const SignUpPhoto = () => {
  const isSubmitting = useIsSubmitting();
  const actionData = useActionData<typeof action>();
  const inputRef = useRef<HTMLInputElement>(null);

  const [form, fields] = useForm({
    id: "sign-up-photo",
    constraint: getFieldsetConstraint(ImageSchema),
    lastSubmission: actionData?.submission,
    onValidate({ formData }) {
      return parse(formData, { schema: ImageSchema });
    },
  });

  const existingImage = Boolean(fields.avatar.defaultValue);
  const [previewImage, setPreviewImage] = useState<string | null>(
    existingImage ? (fields.avatar.defaultValue as string) : null,
  );

  return (
    <>
      <h5 className="text-center text-black text-2xl font-bold leading-loose">
        Upload profile picture
      </h5>
      <p className="mt-1 text-center text-gray-500 text-sm font-light leading-tight">
        (Optional)
      </p>
      <Form
        method="POST"
        encType="multipart/form-data"
        {...form.props}
        className="relative mt-6 flex justify-center items-center flex-col"
      >
        {previewImage ? (
          <div className="flex justify-center items-center relative">
            <img
              src={previewImage}
              alt="user preview"
              className="w-40 h-40 rounded-full border border-slate-200 shadow-sm object-cover"
            />
            <button
              type="button"
              onClick={() => {
                inputRef.current?.click();
              }}
              className="absolute bottom-0 right-0 w-10 h-10 p-2 bg-indigo-500 rounded-lg justify-center items-center flex"
            >
              <PenIcon />
            </button>
            <button
              type="button"
              onClick={() => {
                setPreviewImage(null);
                if (inputRef.current?.value) {
                  inputRef.current.value = "";
                }
              }}
              className="absolute top-0 right-0 w-5 h-5 p-1 bg-red-400 rounded-lg justify-center items-center flex"
            >
              <CrossIcon pathProps={{ className: "stroke-white" }} />
            </button>
          </div>
        ) : (
          <button
            type="button"
            className="w-40 h-40 p-10 bg-violet-50 rounded-full border-2 border-gray-400 border-dashed justify-center items-center flex"
            onClick={() => {
              inputRef.current?.click();
            }}
          >
            <CameraIcon />
          </button>
        )}
        <input
          ref={inputRef}
          aria-label="Image"
          hidden
          onChange={(event) => {
            const file = event.target.files?.[0];

            if (file) {
              const reader = new FileReader();
              reader.onloadend = () => {
                setPreviewImage(reader.result as string);
              };
              reader.readAsDataURL(file);
            } else {
              setPreviewImage(null);
            }
          }}
          accept="image/*"
          {...conform.input(fields.avatar, {
            type: "file",
          })}
        />
        <button
          type="submit"
          className="w-full mt-8 px-12 py-4 bg-indigo-500 rounded-lg justify-center items-center inline-flex text-center text-white text-base font-semibold leading-tight disabled:bg-gray-100 disabled:text-gray-400"
          disabled={isSubmitting}
        >
          Continue
        </button>
      </Form>
    </>
  );
};

export default SignUpPhoto;
