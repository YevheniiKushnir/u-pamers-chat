import { conform, useForm } from "@conform-to/react";
import { getFieldsetConstraint, parse } from "@conform-to/zod";
import {
  type ActionFunctionArgs,
  type LoaderFunctionArgs,
  json,
  redirect,
} from "@remix-run/node";
import { Form, useActionData } from "@remix-run/react";
import invariant from "tiny-invariant";

import facebookImg from "~/assets/images/facebook.png";
import instargamImg from "~/assets/images/instargam.png";
import linkedlnImg from "~/assets/images/linkedln.png";
import skypeImg from "~/assets/images/skype.png";
import telegramImg from "~/assets/images/telegram.png";
import InputField from "~/components/ui/input/Input";
import useIsSubmitting from "~/hooks/useIsSubmitting";
import { getUserProfile, updateUserSocials } from "~/models/user.server";
import { transformSocialsObjectDTO } from "~/utils/common";
import { SocialsSchema } from "~/utils/schema";
import { createUserSession, requireUserId } from "~/utils/session.server";

export const loader = async ({ request }: LoaderFunctionArgs) => {
  const url = new URL(request.url);
  const prevPage = url.searchParams.get("prevPage");

  if (prevPage !== "/signup/photo") {
    return redirect("/signup/photo");
  }

  await requireUserId(request, "/signup");

  return json({});
};

export const action = async ({ request }: ActionFunctionArgs) => {
  const userId = await requireUserId(request);
  const formData = await request.formData();

  const submission = parse(formData, {
    schema: SocialsSchema,
  });

  if (submission.intent !== "submit") {
    return json({ status: "idle", submission } as const);
  }

  if (!submission.value) {
    return json({ status: "error", submission } as const, {
      status: 400,
    });
  }

  const userProfile = await getUserProfile(userId);

  invariant(
    userProfile?.profile,
    `User profile doesn't exit by user id: ${userId}`,
  );

  try {
    const socials = transformSocialsObjectDTO(
      submission.value,
      userProfile.profile?.id,
    );

    await updateUserSocials(userId, socials);

    return createUserSession({
      redirectTo: "/signup/success?prevPage=/signup/socials",
      remember: true,
      request,
      userId,
    });
  } catch (error) {
    throw new Response("Error while updating social links", { status: 500 });
  }
};

const SignUpSocials = () => {
  const isSubmitting = useIsSubmitting();
  const actionData = useActionData<typeof action>();

  const [form, fields] = useForm({
    id: "sign-up-user-socials",
    constraint: getFieldsetConstraint(SocialsSchema),
    lastSubmission: actionData?.submission,
    onValidate({ formData }) {
      return parse(formData, { schema: SocialsSchema });
    },
  });

  return (
    <>
      <h5 className="text-center text-black text-2xl font-bold leading-loose">
        Connect social medias
      </h5>
      <p className="mt-1 text-center text-gray-500 text-sm font-light leading-tight">
        (Optional)
      </p>
      <Form method="POST" className="mt-6" {...form.props}>
        <div className="flex flex-col gap-4">
          <div className="flex gap-3 items-center [&>div]:flex-1">
            <img src={linkedlnImg} alt="Linkedln" className="w-6 h-6" />
            <InputField label="Linkedln" {...conform.input(fields.Linkedin)} />
          </div>
          <div className="flex gap-3 items-center [&>div]:flex-1">
            <img src={instargamImg} alt="Instargam" className="w-6 h-6" />
            <InputField
              label="Instargam"
              {...conform.input(fields.Instagram)}
            />
          </div>
          <div className="flex gap-3 items-center [&>div]:flex-1">
            <img src={telegramImg} alt="Telegram" className="w-6 h-6" />
            <InputField label="Telegram" {...conform.input(fields.Telegram)} />
          </div>
          <div className="flex gap-3 items-center [&>div]:flex-1">
            <img src={facebookImg} alt="Facebook" className="w-6 h-6" />
            <InputField label="Facebook" {...conform.input(fields.Facebook)} />
          </div>
          <div className="flex gap-3 items-center [&>div]:flex-1">
            <img src={skypeImg} alt="Skype" className="w-6 h-6" />
            <InputField label="Skype" {...conform.input(fields.Skype)} />
          </div>
        </div>
        <button
          type="submit"
          className="w-full mt-8 px-12 py-4 bg-indigo-500 rounded-lg justify-center items-center inline-flex text-center text-white text-base font-semibold leading-tight disabled:bg-gray-100 disabled:text-gray-400"
          disabled={isSubmitting}
        >
          Create account
        </button>
      </Form>
    </>
  );
};

export default SignUpSocials;
