import { conform, useForm } from "@conform-to/react";
import { getFieldsetConstraint, parse } from "@conform-to/zod";
import { Message, User } from "@prisma/client";
import { LoaderFunctionArgs, json, redirect } from "@remix-run/node";
import { Form, Link, useLoaderData } from "@remix-run/react";
import {
  useEffect,
  useState,
  useRef,
  useMemo,
  ChangeEventHandler,
} from "react";
import invariant from "tiny-invariant";

import CrossIcon from "~/assets/icons/CrossIcon";
import LetterIcon from "~/assets/icons/LetterIcon";
import MessageTyping from "~/components/messageList/components/MessageTyping";
import MessageList from "~/components/messageList/MessageList";
import { ErrorList } from "~/components/ui/input/Input";
import useIsSubmitting from "~/hooks/useIsSubmitting";
import useMutationObserver from "~/hooks/useMutationObserver";
import useSocket from "~/hooks/useSocket";
import {
  getUserChat,
  getUserUnreadedMessages,
  readMessages,
} from "~/models/chat.server";
import { GroupedByDateMessages, GroupedByUserMessages } from "~/types";
import { throttle } from "~/utils/common";
import { groupMessagesByDate, groupMessagesByUser } from "~/utils/message";
import { ChatSchema } from "~/utils/schema";
import { getUser } from "~/utils/session.server";
import { cn } from "~/utils/styles";

export const loader = async ({ request, params }: LoaderFunctionArgs) => {
  invariant(
    params.chatId,
    `Ohh, it's seems like we can't find chat id: ${params.chatId}`,
  );

  const user = await getUser(request);
  if (!user) return redirect("/signin");

  const [chat, countUnreadedMessages] = await Promise.all([
    getUserChat(user.id, params.chatId),
    getUserUnreadedMessages(user.id, params.chatId),
  ]);
  invariant(chat, `Sorry, we couldn't find chat by id: ${params.chatId}`);

  if (countUnreadedMessages.length !== 0) {
    await readMessages(countUnreadedMessages.map(({ id }) => id));
  }

  return json({
    user,
    chat: {
      id: chat.id,
      messages: chat.messages,
      users: chat.users.reduce((acc: Record<User["id"], User>, u) => {
        acc[u.id] = u;
        return acc;
      }, {}),
      user: chat.users.find((u) => u.id !== user.id)!,
      count: chat._count.messages,
    },
  });
};

const ChatPage = () => {
  const { user, chat } = useLoaderData<typeof loader>();
  const isSubmitting = useIsSubmitting();
  const socket = useSocket();
  const contentRef = useRef<HTMLElement>(null);
  const [userTyping, setUserTyping] = useState(false);
  const [messages, setMessages] = useState<Message[]>(
    chat.messages as unknown as Message[],
  );

  const messagesByDate = useMemo(
    () =>
      groupMessagesByDate(messages).map(([groupKey, groupMessages]) => [
        groupKey,
        groupMessagesByUser(groupMessages),
      ]),
    [messages],
  );

  const [form, fields] = useForm({
    id: "chat-form",
    constraint: getFieldsetConstraint(ChatSchema),
    onValidate({ formData }) {
      return parse(formData, { schema: ChatSchema });
    },
    onSubmit(event, context) {
      event.preventDefault();
      socket?.emit("chat:end:typing", chat.id, user.id, chat.user.id, () => {
        if (context.formData.has("message")) {
          socket?.emit(
            "chat:message",
            chat.id,
            {
              message: context.formData.get("message") as string,
              chatId: chat.id,
              userId: user.id,
            },
            () => {
              socket.emit("user:message", chat.user.id);
            },
          );
          form.ref.current?.reset();
        }
      });
    },
  });

  useEffect(() => {
    if (!socket) return;

    socket.on("connect", () => {
      socket.emit("chat:join", chat.id);
    });

    socket.on("chat:update:typing", (userId, typing) => {
      if (user.id !== userId) {
        setUserTyping(typing);
      }
    });

    socket.on("chat:update:message", (message) => {
      setMessages((prev) => [...prev, message]);
      if (message.userId !== user.id) {
        socket.emit("chat:message:read", chat.id, message.id);
      }
    });

    socket.on("chat:update:message:read", (message) => {
      setMessages((prev) =>
        prev.map((m) => (m.id === message.id ? message : m)),
      );
      // To-do
    });

    return () => {
      socket.off("connect");
      socket.off("chat:update:typing");
      socket.off("chat:update:message");
      socket.off("chat:update:message:read");
    };
  }, [chat.id, user.id, socket]);

  useEffect(() => {
    window.scrollTo({
      left: 0,
      top: document.body.scrollHeight,
      behavior: "instant",
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useMutationObserver(
    contentRef,
    (mutationsList) => {
      for (const mutation of mutationsList) {
        if (mutation.type === "childList") {
          window.scrollTo({
            left: 0,
            top: document.body.scrollHeight,
            behavior: "smooth",
          });
        }
      }
    },
    {
      childList: true,
      subtree: true,
    },
  );

  const handleMessageChange: ChangeEventHandler<HTMLTextAreaElement> = (
    event,
  ) => {
    if (event.target.value.length && !userTyping) {
      throttle(() => {
        socket?.emit("chat:start:typing", chat.id, user.id, chat.user.id);
      }, 5000);
    } else {
      socket?.emit("chat:end:typing", chat.id, user.id, chat.user.id);
    }
  };

  const handleUnfocusEvent = () => {
    socket?.emit("chat:end:typing", chat.id, user.id, chat.user.id);
  };

  return (
    <main className="bg-zinc-100 min-h-full flex flex-col">
      <nav className="sticky top-0 p-4 bg-white shadow">
        <div className="container mx-auto flex max-w-5xl">
          <h2 className="text-black text-center text-base font-semibold leading-tight w-full justify-center">
            {chat.user?.firstName} {chat.user?.lastName}
          </h2>
          <Link to="/chats" className="justify-end">
            <CrossIcon />
          </Link>
        </div>
      </nav>
      <section
        className="container mx-auto py-6 px-4 flex-1 max-w-4xl"
        ref={contentRef}
      >
        <MessageList
          messagesByDate={
            messagesByDate as unknown as GroupedByDateMessages<GroupedByUserMessages>[]
          }
          users={chat.users as unknown as Record<User["id"], User>}
          userId={user.id}
        />
        <MessageTyping photo={chat.user?.photo} isTyping={userTyping} />
      </section>
      <section className="sticky bottom-0 w-full bg-white">
        <div className="container mx-auto max-w-4xl">
          <Form
            method="POST"
            className="px-4 py-3 justify-start items-center gap-4 flex"
            {...form.props}
          >
            <div className="flex flex-col gap-1 flex-1">
              <textarea
                placeholder="Start typing..."
                className="w-full h-10 outline-none resize-none px-3 py-2 bg-white rounded-lg border border-dashed focus:border-solid focus:border-2 focus:border-indigo-500 border-gray-400 justify-start items-center gap-2 flex placeholder:text-gray-500 text-black text-base font-normal leading-tight"
                {...conform.textarea(fields.message)}
                onChange={handleMessageChange}
                onBlur={handleUnfocusEvent}
              />
              <ErrorList
                id={fields.message.id}
                errors={fields.message.errors}
              />
            </div>
            <button
              type="submit"
              aria-label="send message"
              disabled={isSubmitting}
              className={cn("", {
                "opacity-25": isSubmitting,
              })}
            >
              <LetterIcon />
            </button>
          </Form>
        </div>
      </section>
    </main>
  );
};

export default ChatPage;
