const BarCharIcon = () => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="22"
      height="22"
      viewBox="0 0 22 22"
      fill="none"
    >
      <path
        d="M0.333984 0.332764H1.66732V21.6661H0.333984V0.332764Z"
        fill="#4D5267"
      />
      <path
        d="M0.333984 20.3328H21.6673V21.6661H0.333984V20.3328Z"
        fill="#4D5267"
      />
      <path
        d="M13.2324 20.3331H17.2324V2.56641L13.2324 6.99974V20.3331Z"
        fill="#E6674E"
      />
      <path
        d="M7.43262 20.3333H11.4326V7L7.43262 11.4333V20.3333Z"
        fill="#6168E4"
      />
      <path
        d="M1.66602 20.3329H5.66602V11.4329L1.66602 15.8995V20.3329Z"
        fill="#0066CC"
      />
      <path d="M11.4326 7H15.866V20.3333H11.4326V7Z" fill="#9AA0FE" />
      <path
        d="M5.66602 11.4329H10.0993V20.3329H5.66602V11.4329Z"
        fill="#0078FF"
      />
      <path
        d="M17.2324 2.56641H21.6658V20.3331H17.2324V2.56641Z"
        fill="#FFB323"
      />
    </svg>
  );
};

export default BarCharIcon;
