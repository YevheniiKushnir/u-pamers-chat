import { SVGProps, FC } from "react";

interface CrossIconProps {
  svgProps?: SVGProps<SVGSVGElement>;
  pathProps?: SVGProps<SVGPathElement>;
}

const CrossIcon: FC<CrossIconProps> = ({ pathProps, svgProps }) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="16"
      height="16"
      viewBox="0 0 16 16"
      fill="none"
      {...svgProps}
    >
      <path
        d="M13 13L8 8M8 8L3 3M8 8L13 3M8 8L3 13"
        stroke="#1F1F26"
        strokeWidth="1.2"
        strokeLinecap="round"
        strokeLinejoin="round"
        {...pathProps}
      />
    </svg>
  );
};

export default CrossIcon;
