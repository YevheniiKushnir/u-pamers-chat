import { FC, SVGProps } from "react";

interface CheckIconProps {
  svgProps?: SVGProps<SVGSVGElement>;
  pathProps?: SVGProps<SVGPathElement>;
}

const CheckIcon: FC<CheckIconProps> = ({ pathProps, svgProps }) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="none"
      {...svgProps}
    >
      <path
        d="M3.99963 12.0003L8.94963 16.9503L19.5566 6.34326"
        stroke="#1F1F26"
        strokeWidth="1.6"
        strokeLinecap="round"
        strokeLinejoin="round"
        {...pathProps}
      />
    </svg>
  );
};

export default CheckIcon;
