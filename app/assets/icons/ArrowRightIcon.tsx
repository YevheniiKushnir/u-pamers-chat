import { FC, SVGProps } from "react";

interface ArrowRightIconProps {
  svgProps?: SVGProps<SVGSVGElement>;
  pathProps?: SVGProps<SVGPathElement>;
}

const ArrowRightIcon: FC<ArrowRightIconProps> = ({ svgProps, pathProps }) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="none"
      {...svgProps}
    >
      <path
        d="M7.5 3L16.5 12L7.5 21"
        stroke="#1F1F26"
        strokeWidth="1.6"
        strokeLinecap="round"
        strokeLinejoin="round"
        {...pathProps}
      />
    </svg>
  );
};

export default ArrowRightIcon;
