import { FC, SVGProps } from "react";

export interface ArrowLeftIconProps {
  svgProps?: SVGProps<SVGSVGElement>;
  pathProps?: SVGProps<SVGPathElement>;
}

const ArrowLeftIcon: FC<ArrowLeftIconProps> = ({ svgProps, pathProps }) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="16"
      height="16"
      viewBox="0 0 16 16"
      fill="none"
      {...svgProps}
    >
      <path
        d="M11.4004 13L5.40039 8L11.4004 3"
        stroke="#0066CC"
        strokeWidth="1.2"
        strokeLinecap="round"
        strokeLinejoin="round"
        {...pathProps}
      />
    </svg>
  );
};

export default ArrowLeftIcon;
