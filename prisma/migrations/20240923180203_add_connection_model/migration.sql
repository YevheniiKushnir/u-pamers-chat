-- CreateTable
CREATE TABLE "Connection" (
    "id" TEXT NOT NULL,
    "selfConnectionId" TEXT NOT NULL,
    "connectionId" TEXT,
    "userId" TEXT NOT NULL,

    CONSTRAINT "Connection_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "Connection_selfConnectionId_key" ON "Connection"("selfConnectionId");

-- CreateIndex
CREATE UNIQUE INDEX "Connection_userId_key" ON "Connection"("userId");

-- AddForeignKey
ALTER TABLE "Connection" ADD CONSTRAINT "Connection_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE CASCADE;
