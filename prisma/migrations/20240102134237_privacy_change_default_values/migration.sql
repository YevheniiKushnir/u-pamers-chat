-- AlterTable
ALTER TABLE "Privacy" ALTER COLUMN "showAge" SET DEFAULT true,
ALTER COLUMN "showLocation" SET DEFAULT true,
ALTER COLUMN "showDescription" SET DEFAULT true;
