/*
  Warnings:

  - You are about to drop the column `selfConnectionId` on the `Connection` table. All the data in the column will be lost.

*/
-- DropIndex
DROP INDEX "Connection_selfConnectionId_key";

-- AlterTable
ALTER TABLE "Connection" DROP COLUMN "selfConnectionId";
