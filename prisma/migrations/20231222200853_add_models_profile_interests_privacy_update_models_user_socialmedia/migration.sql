/*
  Warnings:

  - You are about to drop the column `userId` on the `SocialMedia` table. All the data in the column will be lost.

*/
-- CreateEnum
CREATE TYPE "InterestTypes" AS ENUM ('Travel', 'Family', 'Health', 'Attractions', 'Promotions', 'Sports', 'Education', 'Dining', 'Office');

-- CreateEnum
CREATE TYPE "UserRole" AS ENUM ('User', 'Admin');

-- DropForeignKey
ALTER TABLE "SocialMedia" DROP CONSTRAINT "SocialMedia_userId_fkey";

-- AlterTable
ALTER TABLE "SocialMedia" DROP COLUMN "userId",
ADD COLUMN     "profileId" TEXT;

-- AlterTable
ALTER TABLE "User" ADD COLUMN     "role" "UserRole" NOT NULL DEFAULT 'User';

-- CreateTable
CREATE TABLE "Profile" (
    "id" TEXT NOT NULL,
    "about" TEXT,
    "location" TEXT,
    "birthdate" TEXT,
    "profileFullFielded" BOOLEAN NOT NULL DEFAULT false,
    "userId" TEXT NOT NULL,

    CONSTRAINT "Profile_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Interests" (
    "id" TEXT NOT NULL,
    "type" "InterestTypes" NOT NULL,
    "selected" BOOLEAN NOT NULL DEFAULT false,
    "profileId" TEXT,

    CONSTRAINT "Interests_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Privacy" (
    "id" TEXT NOT NULL,
    "accountPrivate" BOOLEAN NOT NULL DEFAULT false,
    "showAge" BOOLEAN NOT NULL DEFAULT false,
    "showLocation" BOOLEAN NOT NULL DEFAULT false,
    "showDescription" BOOLEAN NOT NULL DEFAULT false,
    "profileId" TEXT NOT NULL,

    CONSTRAINT "Privacy_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "Profile_userId_key" ON "Profile"("userId");

-- CreateIndex
CREATE UNIQUE INDEX "Privacy_profileId_key" ON "Privacy"("profileId");

-- AddForeignKey
ALTER TABLE "SocialMedia" ADD CONSTRAINT "SocialMedia_profileId_fkey" FOREIGN KEY ("profileId") REFERENCES "Profile"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Profile" ADD CONSTRAINT "Profile_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Interests" ADD CONSTRAINT "Interests_profileId_fkey" FOREIGN KEY ("profileId") REFERENCES "Profile"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Privacy" ADD CONSTRAINT "Privacy_profileId_fkey" FOREIGN KEY ("profileId") REFERENCES "Profile"("id") ON DELETE CASCADE ON UPDATE CASCADE;
