import { PrismaClient } from "@prisma/client";
import bcrypt from "bcryptjs";

const prisma = new PrismaClient();

async function seedToDevelopment() {
  const users = [
    {
      email: "admin@admin.com",
      password: "12345678",
      photo: "https://avatars.githubusercontent.com/u/103437736?v=4",
      firstName: "__admin__",
      lastName: "TEST",
    },
    {
      email: "tester2@admin.com",
      password: "12345678",
      photo:
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQu2whjzwoBz71waeE07wh1L_sfjpdm6IIf7g&usqp=CAU",
      firstName: "__tester__ 2",
      lastName: "TEST",
    },
    {
      email: "tester3@admin.com",
      password: "12345678",
      photo:
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSp3xq6yMAh7HKpnLoT17HDDvOIAJb0u98jPw&usqp=CAU",
      firstName: "__tester__ 3",
      lastName: "TEST",
    },
  ];

  await prisma.user.deleteMany();

  for await (const user of users) {
    await prisma.user.upsert({
      where: { email: user.email },
      create: {
        email: user.email,
        password: {
          create: {
            hash: bcrypt.hashSync(user.password, 10),
          },
        },
        photo: user.photo,
        firstName: user.firstName,
        lastName: user.lastName,
        profile: {
          create: {
            privacy: { create: {} },
          },
        },
      },
      update: {
        email: user.email,
        password: {
          update: {
            hash: bcrypt.hashSync(user.password, 10),
          },
        },
        photo: user.photo,
        firstName: user.firstName,
        lastName: user.lastName,
        profile: {
          upsert: {
            create: {
              privacy: { create: {} },
            },
            update: {},
          },
        },
      },
    });
  }

  const userOne = await prisma.user.findFirst({
    where: { email: users[0].email },
  });
  const userTwo = await prisma.user.findFirst({
    where: { email: users[1].email },
  });
  const userThree = await prisma.user.findFirst({
    where: { email: users[2].email },
  });

  if (userOne && userTwo && userThree) {
    await prisma.chat.deleteMany();

    const chatOne = await prisma.chat.upsert({
      where: { id: userOne?.id },
      create: {
        users: {
          connect: [{ id: userOne.id }, { id: userTwo.id }],
        },
      },
      update: {
        users: {
          connect: [{ id: userOne.id }, { id: userTwo.id }],
        },
      },
    });
    const chatTwo = await prisma.chat.upsert({
      where: { id: userOne?.id },
      create: {
        users: {
          connect: [{ id: userOne.id }, { id: userThree.id }],
        },
      },
      update: {
        users: {
          connect: [{ id: userOne.id }, { id: userThree.id }],
        },
      },
    });

    await prisma.message.deleteMany();
    await prisma.message.createMany({
      data: [
        {
          chatId: chatOne.id,
          message: `test message from ${userOne.firstName} to ${userTwo.firstName} \nohh, i don't see your messages`,
          userId: userOne.id,
          isSended: true,
          isReaded: true,
        },
        {
          chatId: chatOne.id,
          message: `test message from ${userTwo.firstName} to ${userOne.firstName}`,
          userId: userTwo.id,
          isSended: true,
          isReaded: false,
        },
        {
          chatId: chatOne.id,
          message: `test message from ${userTwo.firstName} to ${userOne.firstName} \nhere you are my message`,
          userId: userTwo.id,
          isSended: true,
          isReaded: false,
        },
        {
          chatId: chatTwo.id,
          message: `test message from ${userOne.firstName} to ${userThree.firstName} \nyes, it's second chat`,
          userId: userOne.id,
          isSended: true,
          isReaded: true,
        },
        {
          chatId: chatTwo.id,
          message: `test message from ${userOne.firstName} to ${userThree.firstName} \nit's my first message to you`,
          userId: userOne.id,
          isSended: true,
          isReaded: true,
        },
        {
          chatId: chatTwo.id,
          message: `test message from ${userThree.firstName} to ${userOne.firstName} \nwell, let's test it`,
          userId: userThree.id,
          isSended: true,
          isReaded: false,
        },
      ],
    });
  }
  console.log(`Database has been seeded. 🌱`);
}

async function seedToProduction() {
  const users = [
    {
      email: "admin@admin.com",
      password: process.env.ADMIN_PASSWORD as string,
      photo: "https://avatars.githubusercontent.com/u/103437736?v=4",
      firstName: "__admin__",
      lastName: "(author)",
    },
  ];

  for await (const user of users) {
    await prisma.user.upsert({
      where: { email: user.email },
      create: {
        email: user.email,
        password: {
          create: {
            hash: bcrypt.hashSync(user.password, 10),
          },
        },
        firstName: user.firstName,
        lastName: user.lastName,
        profile: {
          create: {
            privacy: { create: {} },
          },
        },
      },
      update: {
        email: user.email,
        password: {
          update: {
            hash: bcrypt.hashSync(user.password, 10),
          },
        },
        photo: user.photo,
        firstName: user.firstName,
        lastName: user.lastName,
        profile: {
          upsert: {
            create: {
              privacy: { create: {} },
            },
            update: {},
          },
        },
      },
    });
  }

  const usersFromDB = await prisma.user.findMany({
    where: {
      NOT: {
        profile: undefined,
      },
    },
  });
  for await (const user of usersFromDB) {
    await prisma.profile.upsert({
      where: {
        userId: user.id,
      },
      create: {
        userId: user.id,
        privacy: {
          create: {},
        },
      },
      update: {
        privacy: {
          upsert: {
            create: {},
            update: {},
          },
        },
      },
    });
  }
}

let runSeed: () => Promise<void>;

if (process.env.DB_MODE === "production") {
  runSeed = seedToProduction;
} else {
  runSeed = seedToDevelopment;
}

runSeed()
  .catch((e) => {
    console.error(e);
    process.exit(1);
  })
  .finally(async () => {
    await prisma.$disconnect();
  });
